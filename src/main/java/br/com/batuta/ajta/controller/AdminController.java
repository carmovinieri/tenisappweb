package br.com.batuta.ajta.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.entity.Game;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.jsonview.athlete.AthleteAdminEditView;
import br.com.batuta.ajta.jsonview.athlete.AthleteAdminView;
import br.com.batuta.ajta.jsonview.game.PublicGameListView;
import br.com.batuta.ajta.service.AdminService;
import br.com.batuta.ajta.service.AthleteService;

@Controller
@RequestMapping(value = "/adm")
public class AdminController {

	private AthleteService athleteService;
	
	private AdminService adminService; 
	
	@Autowired
	public AdminController(AthleteService athleteService, AdminService adminService) {
		super();
		this.athleteService = athleteService;
		this.adminService = adminService;
	}

	@PreAuthorize("hasAnyRole('ROLE_ADM', 'ROLE_SYSTEM_ADM')")
	@RequestMapping(value="/athlete-list", method=RequestMethod.GET)
	@ResponseBody
	@JsonView(value = AthleteAdminView.class)
	public List<Athlete> listAthletes(@RequestParam(value = "categoryId") Long categoryId) {
		try {
			return athleteService.list(categoryId);
		} catch (Exception e) {
			return new ArrayList<Athlete>();
		}
	}
	
	@JsonView(value = AthleteAdminEditView.class)
	@PreAuthorize("hasAnyRole('ROLE_ADM', 'ROLE_SYSTEM_ADM')")
	@RequestMapping(value="/athlete/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Athlete findAthleteById(@PathVariable Long id) {
		try {
			return athleteService.findById(id);
		} catch (Exception e) {
			return null;
		}
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADM', 'ROLE_SYSTEM_ADM')")
	@RequestMapping(value="/update-athlete", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> updateAthlete(@RequestBody Athlete athlete) {
		try {
			adminService.update(athlete);
		} catch (AjtaException e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<String>(HttpStatus.ACCEPTED);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADM', 'ROLE_SYSTEM_ADM')")
	@RequestMapping(value="/pending-athletes", method=RequestMethod.GET)
	@ResponseBody
	@JsonView(value = AthleteAdminView.class)
	public List<Athlete> listPendingAthletes() {
		List<Athlete> pendingAthletes = new ArrayList<>();
		
		try {
			pendingAthletes = adminService.listPendingAthletes();
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}
		
		return pendingAthletes;
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADM', 'ROLE_SYSTEM_ADM')")
	@RequestMapping(value="/pending-games", method=RequestMethod.GET)
	@ResponseBody
	@JsonView(value = {PublicGameListView.class})
	public List<Game> listPendingGames() {
		List<Game> pendingGames = new ArrayList<>();
		
		try {
			pendingGames = adminService.listPendingGames();
		} catch (AjtaException e) {
			System.out.println(e.getLocalizedMessage());
		}
		
		return pendingGames;
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADM', 'ROLE_SYSTEM_ADM')")
	@RequestMapping(value="/approve-athlete/{id}", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> approveAthlete(@PathVariable(value = "id") Long athleteId) {
		try {
			adminService.approveAthlete(athleteId);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AjtaException e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADM', 'ROLE_SYSTEM_ADM')")
	@RequestMapping(value="/approve-game/{id}", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> approveGame(@PathVariable(value = "id") Long gameId) {
		try {
			adminService.approveGame(gameId);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AjtaException e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADM', 'ROLE_SYSTEM_ADM')")
	@RequestMapping(value="/repprove-game/{id}", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> repproveGame(@PathVariable(value = "id") Long gameId) {
		try {
			adminService.repproveGame(gameId);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AjtaException e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}