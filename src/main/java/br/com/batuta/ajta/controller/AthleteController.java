package br.com.batuta.ajta.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.jsonview.athlete.AthletePublicView;
import br.com.batuta.ajta.jsonview.user.LoggedUserView;
import br.com.batuta.ajta.pojo.RankInfo;
import br.com.batuta.ajta.repository.RankInfoRepository;
import br.com.batuta.ajta.service.AthleteService;

@Controller
@RequestMapping(value="/atleta")
public class AthleteController {
	
	private Logger log = Logger.getLogger(getClass());
	
	@Autowired
	private AthleteService athleteService;
	
	@RequestMapping(value = "/getNotPlayInMounth", method = RequestMethod.GET)
	@ResponseBody
	public List<Athlete> getNotPlayInMounth(Authentication authentication) {
		Athlete athlete = null;
		
		if (authentication != null && authentication.isAuthenticated()) {
			athlete = (Athlete) authentication.getPrincipal();
		}
		
		if (athlete == null) {
			return null;
		}
		
		return athleteService.inputGameWebList(athlete.getCategory().getId(), athlete.getId());
	}
	
	@RequestMapping(method=RequestMethod.GET)
	@ResponseBody
	public List<Athlete> getPlayers(@RequestParam(value="idCategoria") Long idCategoria, @RequestParam(value="idAtleta") Long idAtleta) {
		return athleteService.mobileList(idCategoria, idAtleta);
	}
	
	@JsonView(LoggedUserView.class)
	@RequestMapping(method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Athlete> mobileLogin(@RequestBody Athlete athlete) {
		Athlete databaseAthlete = null;
		
		try {
			databaseAthlete = athleteService.mobileLogin(athlete);
		} catch (AjtaException e) {
			e.printStackTrace();
			return new ResponseEntity<Athlete>(HttpStatus.FORBIDDEN);
		}
		
		return new ResponseEntity<Athlete>(databaseAthlete, HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(value="/perfil", method=RequestMethod.GET)
	@ResponseBody
	public RankInfo getPerfil(@RequestParam(value="idAtleta") Long idAtleta) {
		RankInfo p = new RankInfoRepository().getPerfil(idAtleta);
		return p;
	}
	
	@RequestMapping(value="/inscrever", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Athlete> save(@RequestBody Athlete athlete) {
		try {
			athleteService.save(athlete);
		} catch (AjtaException e) {
			return new ResponseEntity<Athlete>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Athlete>(athlete, HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Athlete> findById(@PathVariable(value="id") Long id) {
		Athlete athlete = null;
		athlete = athleteService.findById(id);
		
		return new ResponseEntity<Athlete>(athlete, HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(value="/athleteList", method=RequestMethod.GET)
	@ResponseBody
	@JsonView(value = AthletePublicView.class)
	public List<Athlete> getAthleteList(@RequestParam(value = "categoryId") Long idCategoria) {
		try {
			return athleteService.list(idCategoria);
		} catch (Exception e) {
			return new ArrayList<Athlete>();
		}
	}
	
	@RequestMapping(value="/approveStatus", method=RequestMethod.PUT)
	@ResponseBody
	public Athlete approveStatus(@RequestBody Athlete athlete) {
		try {
			athleteService.approveStatus(athlete);
			return athlete;
		} catch (Exception e) {
			return athlete;
		}
	}
	
	@RequestMapping(value="/madeAllGamesMonth", method=RequestMethod.GET)
	@ResponseBody
	public boolean madeAllGamesMonth( Authentication authentication ){
		Athlete athlete = null;
		Boolean madeAll = false;
		
		if (authentication != null && authentication.isAuthenticated()) {
			athlete = (Athlete) authentication.getPrincipal();
			try {
				madeAll = athleteService.madeAllGamesOnMounth(athlete.getId());
			} catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
			}
		}
		
		return madeAll;
	}
	
}
