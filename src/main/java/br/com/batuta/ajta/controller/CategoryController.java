package br.com.batuta.ajta.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.batuta.ajta.entity.Category;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.service.CategoryService;

@Controller
@RequestMapping(value = "/categoria")
public class CategoryController {

	private Logger log = Logger.getLogger(getClass());
	
	@Autowired
	private CategoryService categoriaBusiness;
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<Category> list() {
		return categoriaBusiness.listar();
	}

	@RequestMapping(value = "/mobileList", method = RequestMethod.GET)
	@ResponseBody
	public List<Category> mobileList() {
		return categoriaBusiness.listar();
	}
	
	@PreAuthorize("hasRole('ROLE_ADM')")
	@RequestMapping(method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Category> save(@RequestBody Category category) {
		try {
			categoriaBusiness.save(category);
		} catch (AjtaException e) {
			return new ResponseEntity<Category>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Category>(category, HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Category> findById(@PathVariable(value="id") Long id) {
		Category category = null;
		try {
			category = categoriaBusiness.findById(id);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			return new ResponseEntity<Category>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Category>(category, HttpStatus.ACCEPTED);
	}
}
