package br.com.batuta.ajta.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.entity.Game;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.jsonview.game.PublicGameListView;
import br.com.batuta.ajta.service.GameService;

@RestController
@RequestMapping(value="/jogo")
public class GameController {
	
	@Autowired
	private GameService gameService;

	@RequestMapping(method=RequestMethod.POST)
	public Game saveGameMobile(@RequestBody Game game) {
		return gameService.saveMobile(game);
	}
	
	@RequestMapping(value = "/saveGameWeb", method=RequestMethod.POST)
	public Game saveGameWeb(@RequestBody Game game) {
		return gameService.saveWeb(game);
	}
	
	@JsonView(value = {PublicGameListView.class})
	@RequestMapping(value="/gameList", method=RequestMethod.GET)
	@ResponseBody
	public List<Game> list(
			@RequestParam(value="inicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate, 
			@RequestParam(value="fim") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate, 
			@RequestParam(value="idCategoria", required = false) Long categoryId, 
			@RequestParam(value="idAtleta", required = false) Long athleteId,
			@RequestParam(value="statusGame", required = false) Integer statusGame) {
		return gameService.list(startDate, endDate, categoryId, athleteId, statusGame);
	}
	
	@RequestMapping(value="/gameListMobile", method=RequestMethod.GET)
	@ResponseBody
	public List<Game> listMobile(@RequestParam(value="period") Integer period, 
			@RequestParam(value="idCategoria", required = false) Long categoryId, 
			@RequestParam(value="idAtleta", required = false) Long athleteId) {
		return gameService.listToMobile(period, categoryId, athleteId);
	}
	
	@RequestMapping(value="/gameListAproveMobile", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Game>> approveGameMobile(@RequestParam(value="athleteId", required = false) Long athleteId) {
		List<Game> games = gameService.listToAproveMobile(athleteId);
		
		ResponseEntity<List<Game>> response = new ResponseEntity<List<Game>>(games, HttpStatus.OK);
		
		return response;
	}

	@RequestMapping(value="/approveGame", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Game> approveGame(@RequestBody Game game) {
		try {
			gameService.approveGame(game);
		} catch (AjtaException e) {
			return new ResponseEntity<Game>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Game>(HttpStatus.OK);
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value="/user-games", method=RequestMethod.GET)
	@ResponseBody
	public List<Game> list(@AuthenticationPrincipal Athlete user) {
		return gameService.listUserGames(user.getId());
	}
	
	@RequestMapping(value="/gameListToApprove", method=RequestMethod.GET)
	@ResponseBody
	public List<Game> listApproveControllPanel(Authentication authentication) {
		
		Long idCategoria = null;
		
		if (authentication != null && authentication.isAuthenticated()) {
			Athlete athlete = null;
			athlete = (Athlete) authentication.getPrincipal();
			
			if (athlete != null && !athlete.getSystemAdm()) {
				idCategoria = athlete.getCategory().getId();
			}
		}
		
		return gameService.list(null, null, idCategoria, null, Game.STATUS_AGUARDANDO_APROVACAO);
	}
}
