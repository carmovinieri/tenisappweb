package br.com.batuta.ajta.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.jsonview.user.LoggedUserView;
import br.com.batuta.ajta.service.FileService;

@Controller
public class HomeController {
	
	private FileService fileBusiness;
	
	@Autowired
	public HomeController(FileService fileBusiness) {
		super();
		this.fileBusiness = fileBusiness;
	}

	@RequestMapping(value="/", method=RequestMethod.GET)
	public void home(HttpServletResponse response) throws IOException {
		response.sendRedirect("app");
	}

	@JsonView(value= LoggedUserView.class)
	@RequestMapping(value="/login-successful", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Athlete> loginSuccessful(Authentication authentication) throws IOException {
		return new ResponseEntity<Athlete>((Athlete) authentication.getPrincipal(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/login-fail", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Athlete> loginFail() throws IOException {
		return new ResponseEntity<Athlete>(HttpStatus.FORBIDDEN);
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public void apkUpload(@RequestParam(name = "file") MultipartFile uploadFile, HttpServletResponse response) throws IOException {
		try {
			fileBusiness.uploadApk(uploadFile);
			response.sendRedirect("app/#/file-upload?status=success");
		} catch(Exception e) {
			response.sendRedirect("app/#/file-upload?status=fail");
		}
	}
	
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	@ResponseBody
	public void apkDownload(HttpServletResponse response) throws FileNotFoundException, IOException {
		File file = fileBusiness.retrieveApk();
		
		if (file != null) {
			response.setHeader("Content-Disposition", "attachment;filename=" + FileService.FILE_NAME);
			response.setContentType("application/vnd.android.package-archive");
			
			try {
				IOUtils.copy(new FileInputStream(file), response.getOutputStream());
				response.flushBuffer();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
