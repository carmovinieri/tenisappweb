package br.com.batuta.ajta.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.entity.ForgotPasswordIdentifier;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.service.AthleteService;
import br.com.batuta.ajta.service.PasswordService;

@RequestMapping(value = "/password")
@Controller
public class PasswordController {
	
	private PasswordService passwordService;
	
	private AthleteService athleteService;
	
	@Autowired
	public PasswordController(PasswordService passwordService, AthleteService athleteService) {
		super();
		this.passwordService = passwordService;
		this.athleteService = athleteService;
	}

	@RequestMapping(value = "/reset-password", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ForgotPasswordIdentifier> resetPassword(@RequestBody Athlete athlete, 
			@RequestParam(value = "resetPasswordId") String resetPasswordId) {
		try {
			athleteService.resetPassword(athlete, resetPasswordId);
		} catch (AjtaException e) {
			return new ResponseEntity<ForgotPasswordIdentifier>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<ForgotPasswordIdentifier>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/forgot-mail", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ForgotPasswordIdentifier> sendForgotPasswordMail(@RequestBody ForgotPasswordIdentifier forgotPasswordIdentifier) {
		try {
			passwordService.sendMail(forgotPasswordIdentifier);
		} catch (AjtaException e) {
			return new ResponseEntity<ForgotPasswordIdentifier>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<ForgotPasswordIdentifier>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get-mail-identifier", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ForgotPasswordIdentifier> getForgotPasswordIdentifier(@RequestParam(value = "id") String id) {
		ForgotPasswordIdentifier forgotPasswordIdentifier;
		
		try {
			forgotPasswordIdentifier = passwordService.getForgotPasswordIdentifier(id);
		} catch (AjtaException e) {
			return new ResponseEntity<ForgotPasswordIdentifier>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<ForgotPasswordIdentifier>(forgotPasswordIdentifier, HttpStatus.OK);
	}
}
