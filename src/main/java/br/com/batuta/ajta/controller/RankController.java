package br.com.batuta.ajta.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.batuta.ajta.pojo.Rank;
import br.com.batuta.ajta.repository.RankRepository;

@RestController
@RequestMapping(value="/rank")
public class RankController {

	@RequestMapping(method=RequestMethod.GET)
	public List<Rank> getRanking(@RequestParam(value="idCategoria") Long idCategoria) {
		List<Rank> rank = new RankRepository().list(idCategoria); 
		return rank;
	}
}
