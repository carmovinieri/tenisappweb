package br.com.batuta.ajta.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.jsonview.user.LoggedUserView;
import br.com.batuta.ajta.jsonview.user.MyProfileView;
import br.com.batuta.ajta.pojo.RankInfo;
import br.com.batuta.ajta.repository.RankInfoRepository;
import br.com.batuta.ajta.service.AthleteService;

@RequestMapping(value = "/user")
@Controller
public class UserController {

	private final Logger log = Logger.getLogger(getClass());
	
	private AthleteService athleteService;
	
	@Autowired
	public UserController(AthleteService athleteService) {
		super();
		this.athleteService = athleteService;
	}
	
	@JsonView(value= LoggedUserView.class)
	@RequestMapping(value = "/getLoggedUser", method = RequestMethod.GET)
	@ResponseBody
	public Athlete getLoggedUser(Authentication authentication) {
		Athlete athlete = null;
		
		if (authentication != null && authentication.isAuthenticated()) {
			athlete = (Athlete) authentication.getPrincipal();
			Boolean madeAll = false;
			
			athlete = athleteService.findById(athlete.getId());
			
			try {
				madeAll = athleteService.madeAllGamesOnMounth(athlete.getId());
			} catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
			}
			
			athlete.setMadeAll(madeAll);
		}
		
		return athlete;
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/update-profile", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Athlete> updateProfile(@RequestBody Athlete athlete) {
		try {
			athleteService.update(athlete);
		} catch (AjtaException e) {
			return new ResponseEntity<Athlete>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Athlete>(athlete, HttpStatus.ACCEPTED);
	}

	@JsonView(value = MyProfileView.class)
	@RequestMapping(value = "/load-profile", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Athlete> loadProfile(@AuthenticationPrincipal Athlete user) {
		Athlete currentUser = athleteService.findById(user.getId());
		
		return new ResponseEntity<Athlete>(currentUser, HttpStatus.OK);
	}
	
	@RequestMapping(value="/load-rank-info", method=RequestMethod.GET)
	@ResponseBody
	public RankInfo loadRankInfo(@AuthenticationPrincipal Athlete user) {
		RankInfo rankInfo = new RankInfoRepository().getPerfil(user.getId());
		
		rankInfo.setEndereco(user.getAddress());
		rankInfo.setCelular(user.getPhone());
		rankInfo.setCategoria(user.getCategory().getDescricao());
		
		return rankInfo;
	}
}
