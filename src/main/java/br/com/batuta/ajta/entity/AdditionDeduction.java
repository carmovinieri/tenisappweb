package br.com.batuta.ajta.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="adicoes_deducoes")
public class AdditionDeduction {

	public static Integer WO_TORNEIO = 0;
	public static Integer SEMI_TORNEIO = 1;
	public static Integer VICE_TORNEIO = 2;
	public static Integer CAMPEAO_TORNEIO = 3;
	public static Integer PUNICAO = 4;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn (name="id_atleta", nullable=false, foreignKey = @ForeignKey(name="FK_added__atleta"))
	private Athlete athlete;
	
	@Column(name="dt_ocorrencia", nullable=false, updatable=false)
	private Date dateOfOccurrency;
	
	@Column(name="seq_ocorrencia", columnDefinition = "int default 0")
	private int seqOccurrency;
	
	@Column(name="desc_ocorrencia", length=200, nullable = true)
	private String occurrency;
	
	@Column(name="valor", columnDefinition = "int default 0")
	private int value;
	
	@Column(name="obs", length=200, nullable = false)
	private String observation;
	
	@ManyToOne
	@JoinColumn (name="id_game", nullable=true, foreignKey = @ForeignKey(name="FK_added__jogo"))
	private Game game;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn (name="id_categoria", nullable=true, foreignKey = @ForeignKey(name = "fk_added__categoria"))
	private Category category;
	
	public AdditionDeduction() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Athlete getAthlete() {
		return athlete;
	}

	public void setAthlete(Athlete athlete) {
		this.athlete = athlete;
	}

	public Date getDateOfOccurrency() {
		return dateOfOccurrency;
	}

	public void setDateOfOccurrency(Date dateOfOccurrency) {
		this.dateOfOccurrency = dateOfOccurrency;
	}

	public int getSeqOccurrency() {
		return seqOccurrency;
	}

	public void setSeqOccurrency(int seqOccurrency) {
		this.seqOccurrency = seqOccurrency;
	}

	public String getOccurrency() {
		return occurrency;
	}

	public void setOccurrency(String occurrency) {
		this.occurrency = occurrency;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
}
