package br.com.batuta.ajta.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

import br.com.batuta.ajta.enums.Roles;
import br.com.batuta.ajta.jsonview.athlete.AthleteAdminEditView;
import br.com.batuta.ajta.jsonview.athlete.AthleteAdminView;
import br.com.batuta.ajta.jsonview.athlete.AthletePublicView;
import br.com.batuta.ajta.jsonview.game.PublicGameListView;
import br.com.batuta.ajta.jsonview.user.LoggedUserView;
import br.com.batuta.ajta.jsonview.user.MyProfileView;


@Entity
@Table(name="atleta")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Athlete implements UserDetails, Comparable<Athlete>{

	private static final long serialVersionUID = 1L;
	
	@JsonView(value= {LoggedUserView.class, MyProfileView.class, AthleteAdminView.class, AthleteAdminEditView.class})
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	
	@JsonView(value = {MyProfileView.class, AthleteAdminEditView.class})
	@Column(name="rg", length=15, nullable = false)
	private String rg;
	
	@JsonView(value= {LoggedUserView.class, AthletePublicView.class, PublicGameListView.class, MyProfileView.class, 
			AthleteAdminView.class, AthleteAdminEditView.class})
	@Column(name="nome", length=200, nullable = false)
	private String name;
	
	@JsonView(value = {MyProfileView.class, AthleteAdminEditView.class, AthleteAdminView.class})
	@Column(name="email", length=50, nullable = false, unique = true)
	private String email;
	
	@JsonView(value = {LoggedUserView.class, AthleteAdminView.class, AthleteAdminEditView.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn (name="id_categoria", nullable=false, foreignKey = @ForeignKey(name = "fk_atleta__categoria"))
	private Category category;
	
	@JsonView(value = {AthletePublicView.class, AthleteAdminView.class, AthleteAdminEditView.class})
	@Column(name="dt_inclusao", nullable=false, updatable=false)
	private Date dateOfCreation;
	
	@JsonView(value = {AthletePublicView.class, AthleteAdminEditView.class})
	@Column(name="amarela", columnDefinition="boolean default 0")
	private Boolean camisaAmarela;
	
	@JsonView(value= {LoggedUserView.class, AthleteAdminEditView.class})
	@Column(name="administrador", columnDefinition="boolean default 0")
	private Boolean administrador;
	
	@JsonView(value= {LoggedUserView.class, AthleteAdminEditView.class})
	@Column(name="system_adm", columnDefinition="boolean default 0")
	private Boolean systemAdm;
	
	@JsonView(value = {MyProfileView.class, AthleteAdminEditView.class})
	@Column(name = "profissao")
	private String profession;
	
	@JsonView(value = {MyProfileView.class, AthleteAdminEditView.class, AthleteAdminView.class})
	@Column(name = "telefone")
	private String phone;
	
	@JsonView(value = {MyProfileView.class, AthleteAdminEditView.class})
	@Column(name = "endereco")
	private String address;
	
	@JsonView(value = {MyProfileView.class, AthleteAdminEditView.class})
	@Column(name = "dt_nascimento")
	private Date dateOfBirth;
	
	@Column(name = "senha")
	private String password;
	
	@JsonView(value = {AthleteAdminEditView.class})
	@Column(name = "ativo", columnDefinition = "boolean default 0")
	private boolean enabled;
	
	@JsonView(value = {MyProfileView.class, AthleteAdminEditView.class})
	@Column(name = "cpf", length=11)
	private Long cpf;
	
	@JsonView(value = {MyProfileView.class, AthleteAdminEditView.class})
	@Column(name="apelido", length=50)
	private String nickName;
	
	@Transient
	private List<Game> jogos;
	
	@Transient
	private boolean jaJogou;
	
	@JsonView(value= LoggedUserView.class)
	@Transient
	private boolean madeAll;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Date getDateOfCreation() {
		return dateOfCreation;
	}

	public void setDateOfCreation(Date dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public Boolean getCamisaAmarela() {
		return (camisaAmarela != null ? camisaAmarela : false);
	}

	public void setCamisaAmarela(Boolean camisaAmarela) {
		this.camisaAmarela = camisaAmarela;
	}

	public Boolean getAdministrador() {
		return administrador;
	}

	public void setAdministrador(Boolean administrador) {
		this.administrador = administrador;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public List<Game> getJogos() {
		return jogos;
	}

	public void setJogos(List<Game> jogos) {
		this.jogos = jogos;
	}

	public boolean isJaJogou() {
		return jaJogou;
	}

	public void setJaJogou(boolean jaJogou) {
		this.jaJogou = jaJogou;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public Long getCpf() {
		return cpf;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}
	
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@JsonIgnore
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<String> authorities = new ArrayList<>();
		
		if (this.administrador) {
			authorities.add(Roles.ROLE_ADM.toString());
		}
		
		if (this.systemAdm != null && this.systemAdm) {
			authorities.add(Roles.ROLE_SYSTEM_ADM.toString());
		}
		
		return AuthorityUtils.createAuthorityList(authorities.toArray(new String[authorities.size()]));
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return email;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		// TODO Requires an implementation
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		// TODO Requires an implementation
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Requires an implementation
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}
	
	@JsonView(value = {AthletePublicView.class, AthleteAdminView.class})
	public String getEnabledStr() {
		return (enabled ? "Sim" : "Não");
	}
	
	public Boolean getSystemAdm() {
		return systemAdm;
	}

	public void setSystemAdm(Boolean systemAdm) {
		this.systemAdm = systemAdm;
	}
	
	@Override
	public int compareTo(Athlete arg0) {
		return this.name.compareTo(arg0.getName());
	}
	
	public boolean isMadeAll() {
		return madeAll;
	}

	public void setMadeAll(boolean madeAll) {
		this.madeAll = madeAll;
	}
}
