package br.com.batuta.ajta.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

import br.com.batuta.ajta.jsonview.athlete.AthleteAdminEditView;
import br.com.batuta.ajta.jsonview.athlete.AthleteAdminView;
import br.com.batuta.ajta.jsonview.category.MobileCategoryView;
import br.com.batuta.ajta.jsonview.game.PublicGameListView;
import br.com.batuta.ajta.jsonview.user.LoggedUserView;
import lombok.Data;

@Entity
@Table(name="categoria")
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Category {

	@JsonView(value = {LoggedUserView.class, MobileCategoryView.class, AthleteAdminEditView.class})
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	
	@JsonView(value = {LoggedUserView.class, MobileCategoryView.class, PublicGameListView.class, AthleteAdminView.class})
	@Column(name="nome", length=50, nullable = false)
	private String descricao;
	
	@JsonView(value= LoggedUserView.class)
	@Column(name="qtd_jogos_mes", nullable=false)
	private int numeroJogosMes;
	
	@Column(name="status", columnDefinition="boolean default 0")
	private boolean status;
	
	// MASCULINA = 0 e FEMININA = 1
	@Column(name="masc", columnDefinition="boolean default 0")
	private boolean masculina;
	
	@Column(name="dt_inclusao", nullable=false, updatable=false)
	private Date dataInclusao;
	
	public String getSexo() {
		return (masculina ? "Feminina" : "Masculina");
	}
	
	public String toString() {
		return (id+ " - "+ descricao);
	}
}
