package br.com.batuta.ajta.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.batuta.ajta.enums.Features;
import lombok.Data;

@Entity
@Table(name="funcionalidade")
@Data
public class Feature {

	@Id
	private Long id;
	@Column(name="descricao", nullable=false, length=50)
	private String descricao;
	
	@Column(name="dt_inclusao", nullable=false, updatable=false)
	private Date dataInclusao;
	
	public Feature() {
		
	}
	
	public Feature(Features f) {
		this.id = new Long(f.getPermissao());
		this.descricao = f.getFuncionalidade();
		this.dataInclusao = new Date();
	}
}
