package br.com.batuta.ajta.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

import br.com.batuta.ajta.jsonview.game.PublicGameListView;
import lombok.Data;

@Entity
@Table(name="jogo", uniqueConstraints={@UniqueConstraint(columnNames={"id_seq_jogo", "id_adversario", "id_apontador"}, name="UK_jogo__seqAptAd")} )
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Game {

	public static Integer STATUS_AGUARDANDO_APROVACAO = 0;
	public static Integer STATUS_APROVADO = 1;
	public static Integer STATUS_REPROVADO = 2;
	public static Integer STATUS_INVALIDSCORE = 3;
	public static Integer STATUS_MORTHANONEGAME = 4;
	public static Integer STATUS_DATASUPERIOR = 5;
	public static Integer STATUS_ATHLETE_DESATIVADO = 6;
	public static Integer STATUS_GENERIC = 7;
	public static Integer STATUS_GAME_OUT_MOUNTH = 9;
	
	@JsonView(value = {PublicGameListView.class})
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID") 
	private Long id;
	
	@JsonView(value = {PublicGameListView.class})
	@ManyToOne
	@JoinColumn (name="id_apontador", nullable=false, foreignKey = @ForeignKey(name="FK_jogo__apontador"))
	private Athlete apontador;

	@JsonView(value = {PublicGameListView.class})
	@ManyToOne
	@JoinColumn (name="id_adversario", nullable=false, foreignKey = @ForeignKey(name = "FK_jogo__adversario"))
	private Athlete adversario;
	
	@JsonView(value = {PublicGameListView.class})
	@Column(name="dt_jogo", nullable=false, updatable=false)
	private Date dataJogo;
	
	@JsonView(value = {PublicGameListView.class})
	@Column(name="games_pro", columnDefinition = "int default 0")
	private int gamesPro;
	
	@JsonView(value = {PublicGameListView.class})
	@Column(name="games_con", columnDefinition = "int default 0")
	private int gamesCon;
	
	@Column(name="msg", length=140)
	private String mensagem;
	
	@Column(name="lat")
	private String latitude;
	
	@Column(name="lon")
	private String longitude;
	
	@JsonView(value = {PublicGameListView.class})
	@Lob
	@Column (name = "foto", length=4000)
	private byte[] foto;
	
	@JsonView(value = {PublicGameListView.class})
	@Column(name="dt_inclusao", nullable=false, updatable=false)
	private Date dataInclusao;
	
	@Column(name="id_seq_jogo", nullable=false)
	private Long idSeqJogo;
	
	@JsonView(value = {PublicGameListView.class})
	@ManyToOne
	@JoinColumn (name="id_categoria", nullable=false, foreignKey = @ForeignKey(name = "fk_jogo__categoria"))
	private Category categoria;
	
	@Column(name="status_game", columnDefinition = "int default 0")
	private Integer statusGame;
	
	@Column(name="justificativa", length=255)
	private String justificativa;
	
	@Column(name="amarelo_apontador", nullable = true)
	private Integer amareloApontador;
	 
	@Column(name="amarelo_adversario", nullable = true)
	private Integer amareloAdversario;
	
	@JsonView(value = {PublicGameListView.class})
	@Transient
	private Boolean hasFoto;

	
	public Game () {
		this.statusGame = Game.STATUS_AGUARDANDO_APROVACAO;
	}
	
	@JsonView(value = {PublicGameListView.class})
	public String getStatusGameStr() {
		String retorno = "N/A";
		
		switch (statusGame) {
		case 0:
			retorno = "AG. APROVACAO";
			break;
		case 1:
			retorno = "APROVADO";
			break;
		case 2:
			retorno = "REPROVADO";
			break;
		case 3:
			retorno = "PLACAR INVALIDO";
			break;
		case 4:
			retorno = "ATLETAS JA JOGARAM NO MES";
			break;
		case 6:
			retorno = "ATLETA(S) DESATIVADO";
			break;
		case 7:
			retorno = "ERRO GENERICO - VERIFICAR";
			break;
			
		case 8:
			retorno = "ATINGIU 5 DIAS PARA APROVACAO";
			break;
		case 9:
			retorno = "JOGO INVALIDO VIOULOU REGRA DE JOGOS";
			break;
		default:
			retorno = "N/A";
		}
		
		return retorno;
	}
}
