package br.com.batuta.ajta.enums;

public enum Features {

	CRUD_ATLETA(1,"Atleta","","Acesso cadastro atleta"),
	CRUD_ATLETA_EDT(2,"Acesso edicao cadastro atleta"),
	CRUD_CATEGORIA(3,"Categoria","","Acesso cadastro categoria"),
	CRUD_CATEGORIA_EDT(4,"Acesso edicao cadastro categoria"),
	;
	
	private final int permissao;
	
	private String descricao;
	
	private String likMenu;
	
	private String funcionalidade;
	
	private Features(int permissao, String descricao, String linkMenu, String funcionalidade) {
		this.permissao = permissao;
		this.setDescricao(descricao);
		this.setLikMenu(linkMenu);
		this.setFuncionalidade(funcionalidade);
	}
	
	private Features(int permissao,String funcionalidade) {
		this.permissao = permissao;
		this.setFuncionalidade(funcionalidade);
	}

	//Getters
	public int getPermissao() {
		return permissao;
	}

	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getLikMenu() {
		return likMenu;
	}
	public void setLikMenu(String likMenu) {
		this.likMenu = likMenu;
	}
	public String getFuncionalidade() {
		return funcionalidade;
	}
	public void setFuncionalidade(String funcionalidade) {
		this.funcionalidade = funcionalidade;
	}
}
