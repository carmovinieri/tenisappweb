package br.com.batuta.ajta.exception;

public class AfterDateOfServerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2757832374796765280L;
	
	public AfterDateOfServerException() {
		
	}
	
	public AfterDateOfServerException(String message) {
		super(message);
	}

	public AfterDateOfServerException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
