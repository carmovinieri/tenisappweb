package br.com.batuta.ajta.exception;

public class AjtaException extends Exception {

	private static final long serialVersionUID = 1L;

	public AjtaException() {
		
	}
	
	public AjtaException(String message) {
		super(message);
	}

	public AjtaException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
