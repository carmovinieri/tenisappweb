package br.com.batuta.ajta.exception;

public class AthleteDesactiveException extends Exception {

	private static final long serialVersionUID = 2506151442572275489L;
	
	public AthleteDesactiveException() {}
	
	public AthleteDesactiveException(String message) {
		super(message);
	}

	public AthleteDesactiveException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
