package br.com.batuta.ajta.exception;

public class GameInMounthPastException extends Exception {

	private static final long serialVersionUID = 833819158118349082L;
	
	
	public GameInMounthPastException(String message) {
		super(message);
	}

	public GameInMounthPastException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
