package br.com.batuta.ajta.exception;

public class MoreThanOneGameException extends Exception {

	private static final long serialVersionUID = -40716591086827557L;

	public MoreThanOneGameException(String message) {
		super(message);
	}

	public MoreThanOneGameException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
