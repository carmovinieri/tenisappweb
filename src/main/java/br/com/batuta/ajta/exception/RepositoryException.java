package br.com.batuta.ajta.exception;

public class RepositoryException extends AjtaException {

	private static final long serialVersionUID = 1L;

	public RepositoryException() {
		
	}

	public RepositoryException(String message) {
		super(message);
	}
	
	public RepositoryException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
