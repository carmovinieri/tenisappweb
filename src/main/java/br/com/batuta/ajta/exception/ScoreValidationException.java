package br.com.batuta.ajta.exception;

public class ScoreValidationException extends Exception {

	private static final long serialVersionUID = -6034113490669152973L;

	public ScoreValidationException(String message) {
		super(message);
	}

	public ScoreValidationException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
