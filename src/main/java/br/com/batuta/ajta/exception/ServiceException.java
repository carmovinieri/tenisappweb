package br.com.batuta.ajta.exception;

public class ServiceException extends AjtaException {

	private static final long serialVersionUID = 1L;

	public ServiceException() {
		
	}
	
	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
