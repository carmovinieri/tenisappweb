package br.com.batuta.ajta.pojo;

import br.com.batuta.ajta.entity.Athlete;
import lombok.Data;

@Data
public class BlockGamer {

	private Long player;
	private Integer quantidade;
	
	// Construtor(es)
	public BlockGamer() {}
	
	public BlockGamer(Athlete a) {
		this.player = a.getId();
	}
	
	@Override
	public boolean equals(Object o) { 
		if ((o instanceof BlockGamer) && ((BlockGamer) o).getPlayer().equals(this.getPlayer())) { 
			return true; 
		}
		
		return false; 
	}

	
	public int hashCode() {
		return this.player.intValue() * 8;
	}
	
}
