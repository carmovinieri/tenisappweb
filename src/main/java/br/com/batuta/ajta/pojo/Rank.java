package br.com.batuta.ajta.pojo;

import lombok.Data;

@Data
public class Rank  {

	private Long id;
	private Integer position;
	private String nome;
	private Integer vitoria;
	private Integer derrota;
	private Integer jMes;
	private Integer jTemp;
	private Integer pMes;
	private Integer pTemp;
	private Boolean amarela;
	private Integer jTri;
	private Integer pTri;
	private Integer vTri;
	private Integer dTri;
	private Double percT;
	private Double percTri;
	
//	public Float getAproveitamento() {
//		return (this.vitoria > 0 ? ((this.vitoria.floatValue() / this.jTemp.floatValue()) * 100.0f) : 0.0f);
//	}
//
//	@Override
//	public int compareTo(Rank arg0) {
//		
//		if (this.pTri.compareTo(arg0.getPTri()) == 0) {
//			if (this.pTemp.compareTo(arg0.getPTemp()) == 0) {
//				if (this.vitoria.compareTo(arg0.getVitoria()) == 0) {
//					if (this.getAproveitamento().compareTo(arg0.getAproveitamento()) == 0) {
//						return (arg0.getNome().toLowerCase().compareTo(this.nome.toLowerCase()));
//					} else {
//						return (this.getAproveitamento().compareTo(arg0.getAproveitamento()));
//					}
//				} else {
//					return (this.vitoria.compareTo(arg0.getVitoria()));
//				}
//			} else {
//				return (this.pTemp.compareTo(arg0.getPTemp()));
//			}
//		}
//		
//		return (this.pTri.compareTo(arg0.getPTri()));
//	}
	
}
