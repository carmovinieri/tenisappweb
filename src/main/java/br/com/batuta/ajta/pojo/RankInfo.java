package br.com.batuta.ajta.pojo;

import java.util.Date;

import lombok.Data;

@Data
public class RankInfo {

	private String nome;
	private Integer rank;
	private Integer jogosMes;
	private Integer jogosTemp;
	private Integer melhorRank;
	private String melhorVitoria;
	private Date dataInscricao;
	
	private String endereco;
	private String celular;
	private String categoria;
}
