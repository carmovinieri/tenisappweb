package br.com.batuta.ajta.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.entity.Category;
import br.com.batuta.ajta.exception.RepositoryException;
import br.com.batuta.ajta.pojo.BlockGamer;
import br.com.batuta.ajta.system.util.DateUtil;
import br.com.batuta.ajta.system.util.HibernateUtil;
import br.com.batuta.ajta.system.vo.Period;

@Repository
public class AthleteRepository {

	private Logger log = Logger.getLogger(getClass());
	
	public void save(Athlete athlete) throws RepositoryException {
		Session session = HibernateUtil.getSession();
		
		try {
			session.beginTransaction();
			session.saveOrUpdate(athlete);
			session.getTransaction().commit();
		} catch(RuntimeException re) {
			session.getTransaction().rollback();
			log.error("Erro ao commitar a transação");
			log.error(re.getLocalizedMessage(), re);
			throw new RepositoryException(re.getLocalizedMessage(), re);
		} catch(Exception e) {
			log.error(String.format("Erro ao salvar o seguinte atleta: %s", athlete.toString()));
			log.error(e.getLocalizedMessage(), e);
			throw new RepositoryException(e.getLocalizedMessage(), e);
		} finally {
			session.close();
		}
	}
	
	public Athlete findByEmail(String email) throws RepositoryException {
		Session session = HibernateUtil.getSession();
		Athlete athlete = null;
		
		try {
			Query query = session.createQuery("SELECT a FROM Athlete a JOIN FETCH a.category WHERE a.email = :email");
			query.setString("email", email);
			
			athlete = (Athlete) query.uniqueResult();
		} catch(Exception e) {
			log.error(String.format("Erro ao obter o atleta pelo email %s", email));
			log.error(e.getLocalizedMessage(), e);
			throw new RepositoryException(e.getLocalizedMessage(), e);
		} finally {
			session.close();
		}
		
		return athlete;
	}
	
	@SuppressWarnings("unchecked")
	public List<Athlete> mobileList(Long idCategoria, Long idAtleta) {
		StringBuffer sb = new StringBuffer();
		sb.append("select distinct a.id aId, a.nome aNome, a.amarela aAmarela,  \n");
		sb.append("(case when c.qtd_jogos_mes <= \n");
		sb.append("(select count(id) from jogo where status_game in (0,1) and dt_jogo >=:inicio and dt_jogo <=:fim \n");
		sb.append("and id_categoria =:idCategoria and (id_adversario =:idApontador or id_apontador =:idApontador)) then 1 \n");
		sb.append("when c.qtd_jogos_mes <= \n");
		sb.append("(select count(id) from jogo where status_game in (0,1) and dt_jogo >=:inicio and dt_jogo <=:fim \n");
		sb.append("and id_categoria =:idCategoria and (id_adversario = a.id or id_apontador = a.id)) then 1 \n");
		sb.append("when (select count(id_adversario) from jogo \n");
		sb.append("where status_game in (0,1) and id_apontador =:idApontador and dt_jogo >=:inicio and dt_jogo <=:fim \n");
		sb.append("and id_categoria =:idCategoria and id_adversario = a.id) = 1 then 1 \n");
		sb.append("when (select count(id_apontador) from jogo \n");
		sb.append("where status_game in (0,1) and (id_adversario =:idApontador or id_apontador =:idApontador) and dt_jogo >=:inicio and dt_jogo <=:fim \n");
		sb.append("and id_categoria =:idCategoria and (id_adversario = a.id or id_apontador = a.id)) >= 1 then 1 else 0 end) jogou \n");
		sb.append("from atleta a \n");
		sb.append("inner join categoria c on c.id = a.id_categoria \n");
		sb.append("where a.ativo = 1 and a.id_categoria =:idCategoria and a.id not in (:idApontador) \n");
		Session se = HibernateUtil.getSession();
		try {
			Period p = DateUtil.getCurrentDateOfMonth();
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setter
			query.setLong("idCategoria", idCategoria);
			query.setLong("idApontador", idAtleta);
			query.setTimestamp("inicio", p.getStartDate());
			query.setTimestamp("fim", p.getEndDate());
			//Scalar
			query.addScalar("aId", StandardBasicTypes.LONG);						//0
			query.addScalar("aNome", StandardBasicTypes.STRING);					//1
			query.addScalar("aAmarela", StandardBasicTypes.BOOLEAN);				//2
			query.addScalar("jogou", StandardBasicTypes.BOOLEAN);					//3
			
			List<Object[]> objetos = query.list();
			
			List<Athlete> atletas = new ArrayList<Athlete>();
			for (Object[] o : objetos) {
				Athlete a = new Athlete();
				a.setId((o[0] != null ? ((Long) o[0]) : 0l));
				a.setName((o[1] != null ? (o[1].toString()) : ""));
				a.setCamisaAmarela((o[2] != null ? ((Boolean) o[2]) : false));
				a.setJaJogou((o[3] != null ? ((Boolean) o[3]) : false));
				
				atletas.add(a);
			}
			
			return atletas;
		} catch (Exception e) {
			 log.error(e.getLocalizedMessage(), e);
			 return null;
		} finally {
			se.close();
		}
	}
	
	public Athlete getById(Long athleteId) {
		Session se = HibernateUtil.getSession();
		Athlete athlete = null;

		try {
			Query query = se.createQuery("SELECT a FROM Athlete a JOIN FETCH a.category WHERE a.id = :id");
			query.setLong("id", athleteId);
			athlete = (Athlete) query.uniqueResult();
		} catch (Exception e) {
			throw e;
		} finally {
			se.close();
		}
		
		return athlete;
	}
	
	@SuppressWarnings("unchecked")
	public List<Athlete> list(Long idCategoria) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("select distinct \n");
		sb.append("a.id, a.amarela,a.dt_inclusao, a.email, a.nome, a.rg, a.profissao, a.endereco, a.ativo, a.telefone \n");
		sb.append("from atleta a where a.id_categoria =:idCategoria \n");
		
		List<Athlete> atletasListagem = new ArrayList<Athlete>();
		
		Session se = HibernateUtil.getSession();
		try {
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			query.setLong("idCategoria", idCategoria);
			// Scalar
			query.addScalar("id", StandardBasicTypes.LONG);					//0
			query.addScalar("amarela", StandardBasicTypes.BOOLEAN);			//1
			query.addScalar("email", StandardBasicTypes.STRING);			//2
			query.addScalar("nome", StandardBasicTypes.STRING);				//3
			query.addScalar("rg", StandardBasicTypes.STRING);				//4
			query.addScalar("profissao", StandardBasicTypes.STRING);		//5
			query.addScalar("endereco", StandardBasicTypes.STRING);			//6
			query.addScalar("dt_inclusao", StandardBasicTypes.TIMESTAMP);	//7
			query.addScalar("ativo", StandardBasicTypes.BOOLEAN);			//8
			query.addScalar("telefone", StandardBasicTypes.STRING);			//9
			
			List<Object[]> objetos = query.list();
			
			for(Object[] o : objetos) {
				Athlete a = new Athlete();
				a.setId((Long) o[0]);
				a.setCamisaAmarela(o[1] !=null ? (Boolean) o[1] : false);
				a.setEmail(o[2] != null ? o[2].toString() : "");
				a.setName(o[3] != null ? o[3].toString() : "");
				a.setRg(o[4] != null ? o[4].toString() : "");
				a.setProfession(o[5] != null ? o[5].toString() : "");
				a.setAddress(o[6] != null ? o[6].toString() : "");
				a.setDateOfCreation(o[7] != null ? ((Date) o[7]) : null);
				a.setEnabled(o[8] !=null ? (Boolean) o[8] : false);
				a.setPhone(o[9] != null ? o[9].toString() : "");
				
				atletasListagem.add(a);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			se.close();
		}
		
		return atletasListagem;
	}
	
	public Athlete findById(Long id) {
		Athlete athlete = null;
		
		Session se = HibernateUtil.getSession();
		
		try {
			Query query = se.createQuery("SELECT a FROM Athlete a JOIN FETCH a.category WHERE a.id = :id");
			query.setLong("id", id);
			athlete = (Athlete) query.uniqueResult();
		} catch(Exception e){
			throw e;
		} finally {
			se.close();
		}
		
		return athlete;
	}
	
	@SuppressWarnings("unchecked")
	public List<Athlete> searchByChalenger(Long idAthlete, Long idCategory) throws Exception {
		
		StringBuffer sb = new StringBuffer();
		sb.append("select distinct a.id aId, a.nome Nome, a.amarela amarela, a.id_categoria categoria from atleta a \n");
		sb.append("inner join categoria c on c.id = a.id_categoria \n");
		sb.append("where a.id not in ( \n");
		sb.append("select distinct \n");
		sb.append("case when j.id_adversario =:idAthlete then j.id_apontador else j.id_adversario end idAtleta \n");
		sb.append("from jogo j \n");
		sb.append("where j.status_game in (0,1) and (j.id_adversario =:idAthlete or j.id_apontador =:idAthlete) \n");
		sb.append("and j.dt_jogo >=:dtInicio and j.dt_jogo <=:dtFim \n");
		sb.append(") and a.id_categoria =:idCategory and a.id not in (:idAthlete) \n");
		sb.append("and c.qtd_jogos_mes > (select count( distinct id) from jogo where status_game in (0,1) \n");
		sb.append("and dt_jogo >=:dtMesInicio and dt_jogo <=:dtMesFim and (id_adversario = a.id or id_apontador = a.id)) \n");
		sb.append("and a.id not in (select distinct \n");
		sb.append("case when d.id_desafiado =1 then d.id_desafiante else d.id_desafiado end idDesafiador \n");
		sb.append("from desafio d where (d.id_desafiado =:idAthlete or d.id_desafiante =:idAthlete) \n");
		sb.append("and d.dt_inclusao >=:dtMesInicio and d.dt_inclusao <=:dtMesFim) \n");
		
		
		List<Athlete> atletasChalenger = new ArrayList<Athlete>();
		
		Session se = HibernateUtil.getSession();
		try {
			
			Period p = DateUtil.getCurrentDateOfYaer();
			Period m = DateUtil.getCurrentDateOfMonth();
			
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			query.setLong("idAthlete", idAthlete);
			query.setLong("idCategory", idCategory);
			query.setTimestamp("dtInicio", p.getStartDate());
			query.setTimestamp("dtFim", p.getEndDate());
			query.setTimestamp("dtMesInicio", m.getStartDate());
			query.setTimestamp("dtMesFim", m.getEndDate());
			// Scalar
			query.addScalar("aId", StandardBasicTypes.LONG);				//0
			query.addScalar("Nome", StandardBasicTypes.STRING);				//1
			query.addScalar("amarela", StandardBasicTypes.BOOLEAN);			//2
			query.addScalar("categoria", StandardBasicTypes.LONG);			//3
			
			List<Object[]> objetos = query.list();
			
			for (Object[] o : objetos) {
				Athlete a = new Athlete();
				
				a.setId(o[0] != null ? (Long) o[0] : 0);
				a.setName(o[1] != null ? o[1].toString() : "");
				a.setCamisaAmarela(o[2] != null ? (Boolean) o[2] : false);
				
				Category c = new Category();
				c.setId(o[3] != null ? (Long) o[3] : 0);
				
				a.setCategory(c);
				
				atletasChalenger.add(a);
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			se.close();
		}
		
		return atletasChalenger;
	}
	
	public void activeStatus(Athlete atleta) throws Exception {
		StringBuffer sb = new StringBuffer();
		
		sb.append("update atleta set ativo = 1 where id =:idAtleta");
		Session se = HibernateUtil.getSession();
		try {
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			query.setLong("idAtleta", atleta.getId());
			
			// Executa
			se.beginTransaction();
			query.executeUpdate();
			se.getTransaction().commit();
			
		} catch (Exception e) {
			se.getTransaction().rollback();
			throw e;
		} finally {
			se.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Athlete> listToApprove(Long idCategoria) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("select distinct \n");
		sb.append("a.id, a.amarela,a.dt_inclusao, a.email, a.nome, a.rg, a.profissao, a.endereco, a.ativo, a.telefone, \n");
		sb.append("c.id cId, c.nome cDesc \n");
		sb.append("from atleta a \n");
		sb.append("inner join categoria c on c.id = a.id_categoria \n");
		sb.append("where a.ativo = 0 \n");
		if (idCategoria != null && idCategoria > 0) {
			sb.append(" and a.id_categoria =:idCategoria \n");
		}
		
		List<Athlete> atletasListagem = new ArrayList<Athlete>();
		
		Session se = HibernateUtil.getSession();
		try {
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			if (idCategoria != null && idCategoria > 0) {
				query.setLong("idCategoria", idCategoria);
			}
			// Scalar
			query.addScalar("id", StandardBasicTypes.LONG);					//0
			query.addScalar("amarela", StandardBasicTypes.BOOLEAN);			//1
			query.addScalar("email", StandardBasicTypes.STRING);			//2
			query.addScalar("nome", StandardBasicTypes.STRING);				//3
			query.addScalar("rg", StandardBasicTypes.STRING);				//4
			query.addScalar("profissao", StandardBasicTypes.STRING);		//5
			query.addScalar("endereco", StandardBasicTypes.STRING);			//6
			query.addScalar("dt_inclusao", StandardBasicTypes.TIMESTAMP);	//7
			query.addScalar("ativo", StandardBasicTypes.BOOLEAN);			//8
			query.addScalar("telefone", StandardBasicTypes.STRING);			//9
			query.addScalar("cId", StandardBasicTypes.LONG);				//10
			query.addScalar("cDesc", StandardBasicTypes.STRING);			//11
			
			List<Object[]> objetos = query.list();
			
			for(Object[] o : objetos) {
				Athlete a = new Athlete();
				a.setId((Long) o[0]);
				a.setCamisaAmarela(o[1] !=null ? (Boolean) o[1] : false);
				a.setEmail(o[2] != null ? o[2].toString() : "");
				a.setName(o[3] != null ? o[3].toString() : "");
				a.setRg(o[4] != null ? o[4].toString() : "");
				a.setProfession(o[5] != null ? o[5].toString() : "");
				a.setAddress(o[6] != null ? o[6].toString() : "");
				a.setDateOfCreation(o[7] != null ? ((Date) o[7]) : null);
				a.setEnabled(o[8] !=null ? (Boolean) o[8] : false);
				a.setPhone(o[9] != null ? o[9].toString() : "");
				
				Category c = new Category();
				c.setId(o[10] !=null ? (Long) o[10] : 0l);
				c.setDescricao(o[11] != null ? o[11].toString() : "");
				
				a.setCategory(c);
				
				atletasListagem.add(a);
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			se.close();
		}
		
		return atletasListagem;
	}
	public Boolean madeAllGamesOnMounth(Long idAthlete) {
		Boolean yes = true;
		StringBuffer sb = new StringBuffer();
		sb.append("select case when count(distinct j.id) = c.qtd_jogos_mes then 1 else 0 end madeAll from jogo j \n");
		sb.append("inner join categoria c on c.id = j.id_categoria \n");
		sb.append("where j.status_game in (0, 1) and j.dt_jogo >=:dtInicio and j.dt_jogo <=:dtFim  \n");
		sb.append("and (j.id_apontador =:idAthlete or j.id_adversario =:idAthlete) \n");
		
		Session se = HibernateUtil.getSession();
		try {
			Period p = DateUtil.getCurrentDateOfMonth();
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			query.setLong("idAthlete", idAthlete);
			query.setTimestamp("dtInicio", p.getStartDate());
			query.setTimestamp("dtFim", p.getEndDate());
			// Scalar
			query.addScalar("madeAll", StandardBasicTypes.BOOLEAN);
			
			yes = (Boolean)query.uniqueResult(); 
			
		} catch (Exception e) {
			throw e;
		} finally {
			se.close();
		}
		
		return yes;
	}
	
	@SuppressWarnings("unchecked")
	public List<BlockGamer> findPlayersBlocked(Period arg, Long idCategoria, Long idAtleta) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("select count(id) qtd, \n");
		sb.append("(case when id_adversario =:idApontador then id_apontador else id_adversario end) idAtleta \n");
		sb.append("from jogo j where (id_apontador =:idApontador or id_adversario =:idApontador) \n");
		sb.append("and dt_jogo >=:inicio and dt_jogo <=:fim and id_categoria =:idCategoria and j.status_game in (0, 1) \n");
		sb.append("group by (case when id_adversario =:idApontador then id_apontador else id_adversario end) \n");
		
		Session se = HibernateUtil.getSession();
		try {
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			query.setLong("idApontador", idAtleta);
			query.setLong("idCategoria", idCategoria);
			query.setTimestamp("inicio", arg.getStartDate());
			query.setTimestamp("fim", arg.getEndDate());
			// Scalar
			query.addScalar("qtd", StandardBasicTypes.INTEGER);		//0
			query.addScalar("idAtleta", StandardBasicTypes.LONG);	//1
			
			List<Object[]> objts = query.list();
			List<BlockGamer> blockeds = new ArrayList<BlockGamer>();
			
			for (Object[] o : objts) {
				BlockGamer bg = new BlockGamer();
				
				bg.setQuantidade((Integer) o[0]);
				bg.setPlayer((Long) o[1]);
				
				blockeds.add(bg);
			}
			
			return blockeds;
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Athlete> listPendingAthletes() {
		List<Athlete> pendingAthletes = new ArrayList<>();
		
		Session se = HibernateUtil.getSession();

		try {
			Query query = se.createQuery("SELECT a FROM Athlete a JOIN FETCH a.category WHERE a.enabled = false ORDER BY a.dateOfCreation");
			pendingAthletes = query.list();
		} catch(Exception e) {
			throw e;
		} finally {
			se.close();
		}
		
		return pendingAthletes;
	}
	
	public Integer isPlayerYellowShirt(Long athleteId) throws RepositoryException {
		Session session = HibernateUtil.getSession();
		
		try {
			Criteria criteria = session.createCriteria(Athlete.class);
			criteria.add(Restrictions.eq("id", athleteId));
			Athlete athlete = (Athlete) criteria.uniqueResult();
			
			return athlete.getCamisaAmarela() ? 1 : 0;
		} catch(Exception e) {
			throw new RepositoryException(e.getLocalizedMessage(), e);
		} finally {
			session.close();
		}
	}
}
