package br.com.batuta.ajta.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.batuta.ajta.entity.Category;
import br.com.batuta.ajta.exception.RepositoryException;
import br.com.batuta.ajta.system.util.HibernateUtil;

@Repository
public class CategoryRepository {
	
	private final Logger logger = Logger.getLogger(getClass());
	
	@SuppressWarnings("unchecked")
	public List<Category> listar() {
		Session session = HibernateUtil.getSession();
		List<Category> categorias = new ArrayList<Category>();
		
		try {
			Query query = session.createQuery("SELECT c FROM Category c WHERE c.status = true");
			categorias = query.list();
		} catch(Exception e) {
			logger.error("Erro ao obter categorias");
			logger.error(e.getLocalizedMessage(), e);
		}
		
		return categorias;
	}
	
	public void save(Category category) throws RepositoryException {
		Session session = HibernateUtil.getSession();
		
		try {
			session.beginTransaction();
			session.save(category);
			session.getTransaction().commit();
		} catch(RuntimeException re) {
			session.getTransaction().rollback();
			logger.error("Erro ao commitar a transação");
			logger.error(re.getLocalizedMessage(), re);
			throw new RepositoryException(re.getLocalizedMessage(), re);
		} catch(Exception e) {
			logger.error(String.format("Erro ao salvar o seguinte categoria: %s", category.toString()));
			logger.error(e.getLocalizedMessage(), e);
			throw new RepositoryException(e.getLocalizedMessage(), e);
		} finally {
			session.close();
		}
	}
	
	public Category findById(Long idCategory) throws RepositoryException {
		Session session = HibernateUtil.getSession();
		
		try {
			return session.get(Category.class, idCategory);
		} catch(RuntimeException re) {
			session.getTransaction().rollback();
			logger.error("Erro ao commitar a transação");
			logger.error(re.getLocalizedMessage(), re);
			throw new RepositoryException(re.getLocalizedMessage(), re);
		} catch(Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new RepositoryException(e.getLocalizedMessage(), e);
		} finally {
			session.close();
		}
	}
}
