package br.com.batuta.ajta.repository;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.type.StandardBasicTypes;

import br.com.batuta.ajta.entity.Feature;
import br.com.batuta.ajta.enums.Features;
import br.com.batuta.ajta.system.util.HibernateUtil;

public class FeatureRepository {

	public void inserirFuncionalidades() throws Exception {

		List<Feature> featuresToBeCreated = new ArrayList<Feature>();
		
		try {
			for (Features f : Features.values()) {
				
				Feature feature = loadFeature(new Long(f.getPermissao()));
				if (feature == null) {
					featuresToBeCreated.add(new Feature(f));
				} else if (!feature.getDescricao().equals(f.getFuncionalidade())) {
					feature.setDescricao(f.getFuncionalidade());
					atualizaFuncionalidade(feature);
				}
			}
			
			if (featuresToBeCreated != null && featuresToBeCreated.size() > 0) {
				createFeature(featuresToBeCreated);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public Feature loadFeature(Long id) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("select id, descricao from funcionalidade where id =:idFuncionalidade");
		Session se = HibernateUtil.getSession();
		try {
			SQLQuery query = se.createSQLQuery(sb.toString());
			
			// Scalar
			query.addScalar("id", StandardBasicTypes.LONG);
			query.addScalar("descricao", StandardBasicTypes.STRING);
			// Setters
			query.setLong("idFuncionalidade", id);
			
			List<Object[]> objeto = query.list();
			
			if (objeto != null && objeto.size() > 0) {
				Feature f = new Feature();
				f.setId((Long) objeto.get(0)[0]);
				f.setDescricao(objeto.get(0)[1].toString());
				
				return f;
			}
			
			return null;
			
		} catch (Exception e) {
			throw e;
		} finally {
			se.close();
		}
	}
	
	public void atualizaFuncionalidade(Feature f) throws Exception {
		StringBuffer sb = new StringBuffer("update funcionalidade set descricao =:descricao where id =:idFuncionalidade");
		Session se = HibernateUtil.getSession();
		try {
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			query.setString("descricao", f.getDescricao());
			query.setLong("idFuncionalidade", f.getId());
			
			se.beginTransaction();
			query.executeUpdate();
			se.getTransaction().commit();
			
		} catch (Exception e) {
			se.getTransaction().rollback();
			throw e;
		} finally {
			se.close();
		}
	}
	
	public void createFeature(List<Feature> funcionalidads) throws Exception {
		StringBuffer sb = new StringBuffer("insert into funcionalidade (id, descricao, dt_inclusao) values (:id, :descricao, sysdate()) ");
		Session se = HibernateUtil.getSession();
		try {
			SQLQuery query = se.createSQLQuery(sb.toString());
			se.beginTransaction();
			for (Feature f : funcionalidads) {
				// Setters
				query.setLong("id", f.getId());
				query.setString("descricao", f.getDescricao());
				
				query.executeUpdate();
			}
			se.getTransaction().commit();
		} catch (Exception e) {
			se.getTransaction().rollback();
			throw e;
		} finally {
			se.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findCoordinators() throws Exception {
		StringBuffer sb = new StringBuffer();
		
		sb.append("select distinct email from atleta where administrador = 1;");
		Session se = HibernateUtil.getSession();
		try {
			SQLQuery query = se.createSQLQuery(sb.toString());
			
			// Executa
			return (List<String>) query.list();
		} catch (Exception e) {
			se.getTransaction().rollback();
			throw e;
		} finally {
			se.close();
		}
	}
}
