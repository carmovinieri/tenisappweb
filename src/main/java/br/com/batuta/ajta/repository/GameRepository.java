package br.com.batuta.ajta.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.entity.Category;
import br.com.batuta.ajta.entity.Game;
import br.com.batuta.ajta.exception.RepositoryException;
import br.com.batuta.ajta.system.util.HibernateUtil;

@Repository
public class GameRepository {

	private Logger log = Logger.getLogger(getClass());
	
	public Game findById(Long gameId) throws RepositoryException {
		Session session = HibernateUtil.getSession();
		
		Game game = null;
		
		try {
			game = session.get(Game.class, gameId);
		} catch (Exception e) {
			throw new RepositoryException(e.getLocalizedMessage(), e);
		} finally {
			session.close();
		}
		
		return game;
	}
	
	/**
	 * Metodo save
	 * Persiste no banco o jogo enviado pelo mobile.
	 * Utilizado também caso aja uma edição no WEB.
	 * 
	 * @param jogo
	 * Jogo enviado pelo mobile.
	 * 
	 * @return
	 * Jogo persistido em banco.
	 */
	public Game save(Game jogo) {
		Session se = HibernateUtil.getSession();
		
		try {
			se.beginTransaction();
			se.saveOrUpdate(jogo);
			se.getTransaction().commit();
		} catch (Exception e) {
			se.getTransaction().rollback();
			log.error(e.getLocalizedMessage(), e);
		} finally {
			se.close();
		}
		
		return jogo;
	}
	
	/**
	 * Responsavel por validar se o jogo fora enviado mais de uma vez pelo mobile.
	 * (Pode ocorrer devido a oscilação da internet)
	 * 
	 * @param idSeqJogo
	 * Sequencial do jogo.
	 * 
	 * @param athleteId
	 * Id do atleta.
	 * 
	 * @return
	 * Jogo persistido no banco.
	 */
	public Game loadBySeqAndAtleta(Long idSeqJogo, Long athleteId) {
		Session se = HibernateUtil.getSession();
		
		Game game = null;
		
		try {
			Query query = se.createQuery("SELECT j FROM Game j WHERE j.idSeqJogo = :idSeqJogo AND j.apontador.id = :athleteId");
			query.setLong("idSeqJogo", idSeqJogo);
			query.setLong("athleteId", athleteId);
			
			game = (Game) query.uniqueResult();
		} catch (Exception e) {
			log.error(String.format("Erro ao carregar jogo pelos idSeqJogo %s e Atleta %s", idSeqJogo, athleteId));
			log.error(e.getLocalizedMessage(), e);
		} finally {
			se.close();
		}
		
		return game;
	}
	
	/**
	 * Metodo list
	 * 
	 * Utilisado pela web para listar os jogos.
	 * 
	 * @param start
	 * Data inicio da pesquisa.
	 * 
	 * @param finish
	 * Data fim da pesquisa.
	 * 
	 * @param idCategoria
	 * Categoria (não obrigatorio).
	 * 
	 * @param idAtleta
	 * Atleta (não obrigatorio).
	 * 
	 * @return
	 * Lista com os jogos encontrados.
	 */
	@SuppressWarnings("unchecked")
	public List<Game> list(Date start, Date finish, Long idCategoria, Long idAtleta, Integer statusGame) {
		StringBuffer sb = new StringBuffer();
		sb.append("select distinct ");
		sb.append("j.id jId, j.dt_jogo dtJogo, j.dt_inclusao dtIncusao, j.foto foto,j.games_pro gamesApont, j.games_con gamesAdver, \n");
		sb.append("a.id aId,a.nome apontador, d.id dId,d.nome adversario, j.status_game statusGame, c.id cId, c.nome cDesc ");
		sb.append("from jogo j \n");
		sb.append("inner join atleta a on a.id = j.id_apontador \n");
		sb.append("inner join atleta d on d.id = j.id_adversario \n");
		sb.append("inner join categoria c on c.id = j.id_categoria \n");
		sb.append("where 1= 1 \n");
		if (start != null) {
			sb.append(" and j.dt_jogo >=:inicio \n");
		}
		if (finish != null) {
			sb.append(" and j.dt_jogo <=:fim \n");
		}
		
		if (idCategoria != null && idCategoria > 0) {
			sb.append(" and j.id_categoria =:idCategoria \n");
		}
		
		if (idAtleta != null && idAtleta > 0) {
			sb.append(" and (a.id =:idAtleta or d.id =:idAtleta) \n");
		}
		
		if (statusGame != null) {
			sb.append(" and j.status_game =:statusGame \n");
		}
		
		sb.append("order by j.dt_jogo desc \n");
		
		Session se = HibernateUtil.getSession();
		List<Game> jogos = new ArrayList<Game>();
		
		try {
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			if (start != null) {
				query.setTimestamp("inicio", start);
			}
			if (finish != null) {
				query.setTimestamp("fim", finish);
			}
			if (idCategoria != null && idCategoria > 0) {
				query.setLong("idCategoria", idCategoria);
			}
			if (idAtleta != null && idAtleta > 0) {
				query.setLong("idAtleta", idAtleta);
			}
			
			if (statusGame != null) {
				query.setInteger("statusGame", statusGame);
			}
			
			// Scalar
			query.addScalar("jId", StandardBasicTypes.LONG);			//0
			query.addScalar("dtJogo", StandardBasicTypes.TIMESTAMP);	//1
			query.addScalar("dtIncusao", StandardBasicTypes.TIMESTAMP);	//2
			query.addScalar("foto", StandardBasicTypes.BINARY);			//3
			query.addScalar("gamesApont", StandardBasicTypes.INTEGER);	//4
			query.addScalar("gamesAdver", StandardBasicTypes.INTEGER);	//5
			query.addScalar("aId", StandardBasicTypes.LONG);			//6
			query.addScalar("apontador", StandardBasicTypes.STRING);	//7
			query.addScalar("dId", StandardBasicTypes.LONG);			//
			query.addScalar("adversario", StandardBasicTypes.STRING);	//9
			query.addScalar("statusGame", StandardBasicTypes.INTEGER);	//10
			query.addScalar("cId", StandardBasicTypes.LONG);			//11
			query.addScalar("cDesc", StandardBasicTypes.STRING);		//12

			List<Object[]> objetos = query.list();
			
			for (Object[] o : objetos) {
				Game j = new Game();
				j.setId((Long) o[0]);
				j.setDataJogo((Date) o[1]);
				j.setDataInclusao((Date) o[2]);
				j.setFoto((byte[]) o[3]);
				j.setGamesPro((Integer) o[4]);
				j.setGamesCon((Integer) o[5]);
				j.setStatusGame((Integer) o[10]);
				
				// Apontador
				Athlete a = new Athlete();
				a.setId((Long) o[6]);
				a.setName(o[7].toString());
				j.setApontador(a);
				
				// Adversario
				Athlete d = new Athlete();
				d.setId((Long) o[8]);
				d.setName(o[9].toString());
				j.setAdversario(d);
				
				// Categoria
				Category c = new Category();
				c.setId((Long) o[11]);
				c.setDescricao(o[12].toString());
				
				j.setCategoria(c);
				
				jogos.add(j);
			}
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		} finally {
			se.close();
		}
		return jogos;
	}
	
	/**
	 * Metodo listToAproveMobile
	 * 
	 * Responsavel por aprovar um jogo. Lista para o celular todos os jogos não aprovados pelo atleta que forá apontado.
	 * 
	 * @param idAtleta
	 * Atleta apontado.
	 * 
	 * @return
	 * Lista com os jogos encontrados.
	 */
	@SuppressWarnings("unchecked")
	public List<Game> listToAproveMobile(Long idAtleta) {
		if (idAtleta == null || idAtleta <=0) {
			return new ArrayList<Game>();
		}
		StringBuffer sb = new StringBuffer();
		sb.append("select distinct ");
		sb.append(" j.id jId, j.dt_jogo dtJogo, j.games_pro gamesApont, j.games_con gamesAdver,(case when (j.foto is null) then 0 else 1 end) hasFoto, ");
		sb.append("a.id aId,a.nome apontador from jogo j ");
		sb.append("inner join atleta a on a.id = j.id_apontador ");
		sb.append(" where j.id_adversario =:idAtleta and j.status_game = 0 ");
		Session se = HibernateUtil.getSession();
		List<Game> jogos = new ArrayList<Game>();
		
		try {
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			query.setLong("idAtleta", idAtleta);
			// Scalar
			query.addScalar("jId", StandardBasicTypes.LONG);			//0
			query.addScalar("dtJogo", StandardBasicTypes.TIMESTAMP);	//1
			query.addScalar("gamesApont", StandardBasicTypes.INTEGER);	//2
			query.addScalar("gamesAdver", StandardBasicTypes.INTEGER);	//3
			query.addScalar("aId", StandardBasicTypes.LONG);			//4
			query.addScalar("apontador", StandardBasicTypes.STRING);	//5
			query.addScalar("hasFoto", StandardBasicTypes.BOOLEAN);		//6
			
			List<Object[]> objetos = query.list();
			
			for (Object[] o : objetos) {
				Game j = new Game();
				j.setId((Long) o[0]);
				j.setDataJogo((Date) o[1]);
				j.setGamesPro((Integer) o[2]);
				j.setGamesCon((Integer) o[3]);
				j.setHasFoto((Boolean) o[6]);
				
				// Apontador
				Athlete a = new Athlete();
				a.setId((Long) o[4]);
				a.setName(o[5].toString());
				j.setApontador(a);
				
				jogos.add(j);
			}
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		} finally {
			se.close();
		}
		return jogos;
	}

	/**
	 * Metodo approveGame
	 * 
	 * Responsavel por aprovar ou reprovar os jogos.
	 * 
	 * @param game
	 * Jogo a ser aprovado.
	 * 
	 * @throws RepositoryException
	 */
	public void approveGame(Game game) throws RepositoryException {
		StringBuffer sb = new StringBuffer("update jogo set status_game =:statusGame, justificativa=:justificativa where id =:idGame");

		Session se = HibernateUtil.getSession();
		try {
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			query.setInteger("statusGame", game.getStatusGame());
			query.setString("justificativa", game.getJustificativa());
			query.setLong("idGame", game.getId());

			se.beginTransaction();
			query.executeUpdate();
			se.getTransaction().commit();
		} catch(RuntimeException re) {
			se.getTransaction().rollback();
			log.error(String.format("Erro durante a transação do jogo %s", game.getId()));
			log.error(re.getLocalizedMessage(), re);
			throw new RepositoryException(re.getLocalizedMessage(), re);
		} catch (Exception e) {
			log.error(String.format("Erro durante a transação do jogo %s", game.getId()));
			log.error(e.getLocalizedMessage(), e);
			throw new RepositoryException(e.getLocalizedMessage(), e);
		} finally {
			se.close();
		}
	}
	
	@SuppressWarnings({ "unchecked" })
	public List<Game> listPendingGames() throws RepositoryException {
		List<Game> games = new ArrayList<>();
		
		Session session = HibernateUtil.getSession();
		try {
			Criteria criteria = session.createCriteria(Game.class);
			criteria
				.add(Restrictions.eq("statusGame", Game.STATUS_AGUARDANDO_APROVACAO))
				.addOrder(Order.asc("categoria.id"))
				.addOrder(Order.desc("dataJogo"));
			games = criteria.list();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new RepositoryException(e.getLocalizedMessage(), e);
		} finally {
			session.close();
		}
		
		return games;
	}
}
