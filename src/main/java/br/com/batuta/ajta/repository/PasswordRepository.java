package br.com.batuta.ajta.repository;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.batuta.ajta.entity.ForgotPasswordIdentifier;
import br.com.batuta.ajta.exception.RepositoryException;
import br.com.batuta.ajta.system.util.HibernateUtil;

@Repository
public class PasswordRepository {

	private final Logger log = Logger.getLogger(getClass());
	
	public ForgotPasswordIdentifier save(ForgotPasswordIdentifier forgotPasswordIdentifier) throws RepositoryException {
		Session session = HibernateUtil.getSession();
		
		try {
			session.beginTransaction().begin();
			session.save(forgotPasswordIdentifier);
			session.getTransaction().commit();
		} catch(RuntimeException re) {
			session.getTransaction().rollback();
			log.error("Erro ao commitar a transação");
			log.error(re.getLocalizedMessage(), re);
			throw new RepositoryException(re.getLocalizedMessage(), re);
		} catch(Exception e) {
			log.error(String.format("Erro ao salvar o seguinte registro de reset de senha: %s", forgotPasswordIdentifier.toString()));
			log.error(e.getLocalizedMessage(), e);
			throw new RepositoryException(e.getLocalizedMessage(), e);
		} finally {
			session.close();
		}
		
		return forgotPasswordIdentifier;
	}
	
	public ForgotPasswordIdentifier findEnabledById(String id) {
		Session session = HibernateUtil.getSession();
		
		ForgotPasswordIdentifier forgotPasswordIdentifier = null;
		
		try {
			Query query = session.createQuery("SELECT f FROM ForgotPasswordIdentifier f WHERE f.id = :id AND f.isEnabled = true");
			query.setString("id", id);
			forgotPasswordIdentifier = (ForgotPasswordIdentifier) query.uniqueResult();
		} catch(Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		
		return forgotPasswordIdentifier;
	}
	
	public void disableIds(String email) {
		Session session = HibernateUtil.getSession();
		
		try {
			session.getTransaction().begin();
			
			Query query = session.createQuery("UPDATE ForgotPasswordIdentifier f set f.isEnabled = false WHERE f.email = :email");
			query.setString("email", email);
			query.executeUpdate();
			
			session.getTransaction().commit();
		} catch(RuntimeException re) {
			session.getTransaction().rollback();
			log.error(String.format("Erro ao commitar a transação de desativação dos ids do reset de senha do email %s", email));
			log.error(re.getLocalizedMessage(), re);
		} catch(Exception e) {
			log.error(String.format("Erro ao resetar os ids do reset de senha do email", email));
			log.error(e.getLocalizedMessage(), e);
		} finally {
			session.close();
		}
	}
}
