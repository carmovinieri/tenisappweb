package br.com.batuta.ajta.repository;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.pojo.RankInfo;
import br.com.batuta.ajta.pojo.Rank;

public class RankInfoRepository {

	private Logger log = Logger.getLogger(getClass());
	
	public RankInfo getPerfil(Long idAtleta) {
		RankInfo p = new RankInfo();
		try {
			Athlete a = new AthleteRepository().getById(idAtleta);
			p.setDataInscricao(a.getDateOfCreation());
			p.setNome(a.getName());
			p.setMelhorRank(0);
			p.setMelhorVitoria("-");
			List<Rank> ranks = new RankRepository().list(a.getCategory().getId());
			
			for (Rank r : ranks) {
				if (r.getId().equals(a.getId())) {
					p.setJogosMes(r.getJMes());
					p.setJogosTemp(r.getJTemp());
					p.setRank(r.getPosition());
					
					break;
				}
			}
			
		} catch (Exception e) {
			 log.error(e.getLocalizedMessage(), e);
			 return null;
		} 
		return p;
	}
}
