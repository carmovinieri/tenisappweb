package br.com.batuta.ajta.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.type.StandardBasicTypes;

import br.com.batuta.ajta.pojo.Rank;
import br.com.batuta.ajta.system.util.DateUtil;
import br.com.batuta.ajta.system.util.HibernateUtil;
import br.com.batuta.ajta.system.vo.Period;

public class RankRepository {

	private Logger log = Logger.getLogger(getClass());
	
	@SuppressWarnings("unchecked")
	public List<Rank> list(Long idCategoria) {
		StringBuffer sb = new StringBuffer();

		sb.append("select r.apont, r.nome, r.v, r.d, r.jm, (r.vTri + r.dTri) jTri, (r.v + r.d) j, (r.pTemp + ifnull(ad.valor,0)) pTemp, r.pMes,r.pTri, r.amarela, \n");
		sb.append("(case when ((r.vTri + r.dTri) = 0) then 0 else ((r.vTri /(r.vTri + r.dTri))*100) end) percTri, \n");
		sb.append("(case when ((r.v + r.d) = 0) then 0 else ((r.v / (r.v + r.d))*100) end) percT, \n");
		sb.append(" r.vTri, r.dTri  from ( \n");
		sb.append("select distinct a.id apont, a.nome, a.amarela, ifnull(sum(rank.vitoria), 0) v, ifnull(sum(rank.derrotas),0) d, \n");
		sb.append("ifnull(sum(rankTri.vTri),0) vTri, ifnull(sum(rankTri.dTri),0) dTri,ifnull(jMes.jm, 0) jm, \n");	
		// Pontos Temporada
		sb.append("ifnull( \n");
		sb.append("(select sum((case \n");
		sb.append("when (j.games_con < j.games_pro and id_apontador = rank.apont) then ((j.games_pro + (j.games_pro - j.games_con)) + (case when j.foto is not null then 2 else 0 end) + (case when j.amarelo_apontador = 1 then 3 else 0 end)) \n");
		sb.append("when (j.games_con > j.games_pro and id_apontador = rank.apont) then (j.games_pro + (case when j.foto is not null then 2 else 0 end) + (case when j.amarelo_apontador = 1 then 2 else 0 end)) \n");
		sb.append("when (j.games_con < j.games_pro and id_adversario = rank.apont) then (j.games_con + (case when j.foto is not null then 2 else 0 end) + (case when j.amarelo_adversario = 1 then 2 else 0 end)) \n");
		sb.append("when (j.games_con > j.games_pro and id_adversario = rank.apont) then ((j.games_con + (j.games_con - j.games_pro)) + (case when j.foto is not null then 2 else 0 end) + (case when j.amarelo_adversario = 1 then 3 else 0 end)) end ))  \n");
		sb.append("from jogo j where status_game = 1 and j.dt_jogo >=:dtIniTemp and j.dt_jogo <=:dtFimTemp and j.id_categoria =:idCategoria and (id_apontador = rank.apont or id_adversario = rank.apont)),0) pTemp, \n");
		
		// Pontos Mes
		sb.append("ifnull((select sum((case \n");
		sb.append("when (j.games_con < j.games_pro and id_apontador = rank.apont) then ((j.games_pro + (j.games_pro - j.games_con)) + (case when j.foto is not null then 2 else 0 end) + (case when j.amarelo_apontador = 1 then 3 else 0 end)) \n");
		sb.append("when (j.games_con > j.games_pro and id_apontador = rank.apont) then (j.games_pro + (case when j.foto is not null then 2 else 0 end) + (case when j.amarelo_apontador = 1 then 2 else 0 end)) \n");
		sb.append("when (j.games_con < j.games_pro and id_adversario = rank.apont) then (j.games_con + (case when j.foto is not null then 2 else 0 end) + (case when j.amarelo_adversario = 1 then 2 else 0 end)) \n");
		sb.append("when (j.games_con > j.games_pro and id_adversario = rank.apont) then ((j.games_con + (j.games_con - j.games_pro)) + (case when j.foto is not null then 2 else 0 end) + (case when j.amarelo_adversario = 1 then 3 else 0 end)) end )) \n");
		sb.append("from jogo j where status_game = 1 and j.id_categoria =:idCategoria and (id_apontador = rank.apont or id_adversario = rank.apont) \n");
		sb.append("and j.dt_jogo >=:inicio and j.dt_jogo <=:fim ),0) pMes,  \n");
		
		// Pontos Trimestre
		sb.append("ifnull((select sum((case \n");
		sb.append("when (j.games_con < j.games_pro and id_apontador = rank.apont) then ((j.games_pro + (j.games_pro - j.games_con)) + (case when j.foto is not null then 2 else 0 end) + (case when j.amarelo_apontador = 1 then 3 else 0 end)) \n");
		sb.append("when (j.games_con > j.games_pro and id_apontador = rank.apont) then (j.games_pro + (case when j.foto is not null then 2 else 0 end) + (case when j.amarelo_apontador = 1 then 2 else 0 end)) \n");
		sb.append("when (j.games_con < j.games_pro and id_adversario = rank.apont) then (j.games_con + (case when j.foto is not null then 2 else 0 end) + (case when j.amarelo_adversario = 1 then 2 else 0 end))  \n");
		sb.append("when (j.games_con > j.games_pro and id_adversario = rank.apont) then ((j.games_con + (j.games_con - j.games_pro)) + (case when j.foto is not null then 2 else 0 end) + (case when j.amarelo_adversario = 1 then 3 else 0 end)) end )) \n");
		sb.append("from jogo j where status_game = 1 and j.id_categoria =:idCategoria and (id_apontador = rank.apont or id_adversario = rank.apont) \n");
		sb.append("and j.dt_jogo >=:iniTri and j.dt_jogo <=:fimTri ),0) pTri \n");
		sb.append("from atleta a left join \n");
		sb.append("(select distinct APT.apont, (ifnull(APT.vitoria,0) + ifnull(ADV.vitoria,0)) vitoria, (ifnull(APT.derrotas,0) + ifnull(ADV.derrotas,0)) derrotas from \n");
		sb.append("((select a.id apont, ifnull(sum(tbl.v),0) vitoria, ifnull(sum(tbl.d),0) derrotas from atleta a left join \n");
		sb.append("(select	j.id_apontador apont, (case when (j.games_con > j.games_pro) then 1 else 0 end ) d, \n");
		sb.append("(case when (j.games_con < j.games_pro) then 1 else 0 end ) v from jogo j where status_game = 1 and j.id_categoria =:idCategoria   \n");
		sb.append("and j.dt_jogo >=:dtIniTemp and j.dt_jogo <=:dtFimTemp) tbl on a.id = tbl.apont where a.id_categoria =:idCategoria group by a.id) APT");
		sb.append(" left join \n");
		sb.append("(select a.id apont, ifnull(sum(tbl.v),0) vitoria, ifnull(sum(tbl.d),0) derrotas from atleta a left join \n");
		sb.append("(select j.id_adversario apont, (case when (j.games_con < j.games_pro) then 1 else 0 end ) d, \n");
		sb.append("(case when (j.games_con > j.games_pro) then 1 else 0 end ) v from jogo j where j.status_game = 1 and j.id_categoria =:idCategoria \n");
		sb.append("and j.dt_jogo >=:dtIniTemp and j.dt_jogo <=:dtFimTemp) tbl on a.id = tbl.apont where a.id_categoria =:idCategoria group by a.id) ADV on ADV.apont = APT.apont )) rank on a.id = rank.apont \n");
		sb.append("left join ( select count(j.id) jm, ap.id idAtleta from jogo j inner join atleta ap on (ap.id = j.id_apontador or ap.id = j.id_adversario) \n");
		sb.append("where j.status_game = 1 and j.id_categoria =:idCategoria and j.dt_jogo >=:inicio and j.dt_jogo <=:fim group by ap.id) jMes on rank.apont = jMes.idAtleta \n");
		sb.append("left join \n");
		sb.append("(select distinct triApt.apont, (ifnull(triApt.vitoria,0) + ifnull(triAdv.vitoria,0)) vTri, (ifnull(triApt.derrotas,0) + ifnull(triAdv.derrotas,0)) dTri from \n");
		sb.append("((select a.id apont, ifnull(sum(tbl.v),0) vitoria, ifnull(sum(tbl.d),0) derrotas from atleta a left join \n");
		sb.append("(select	j.id_apontador apont, (case when (j.games_con > j.games_pro) then 1 else 0 end ) d, \n");
		sb.append("(case when (j.games_con < j.games_pro) then 1 else 0 end ) v from jogo j where status_game = 1 and j.id_categoria =:idCategoria \n");
		sb.append("and j.dt_jogo >=:iniTri and j.dt_jogo <=:fimTri) tbl on a.id = tbl.apont where a.id_categoria =:idCategoria group by a.id) triApt \n");
		sb.append("left join (select a.id apont, ifnull(sum(tbl.v),0) vitoria, ifnull(sum(tbl.d),0) derrotas from atleta a left join \n");
		sb.append("(select j.id_adversario apont, (case when (j.games_con < j.games_pro) then 1 else 0 end ) d, \n");
		sb.append("(case when (j.games_con > j.games_pro) then 1 else 0 end ) v from jogo j where j.status_game = 1 and j.id_categoria =:idCategoria \n");
		sb.append("and j.dt_jogo >=:iniTri and j.dt_jogo <=:fimTri) tbl on a.id = tbl.apont where a.id_categoria =:idCategoria group by a.id) \n");
		sb.append("triAdv on triAdv.apont = triApt.apont )) rankTri on a.id = rankTri.apont \n");
		
		sb.append("where a.ativo = 1 and a.id_categoria =:idCategoria \n");
		sb.append("group by rank.apont, a.nome, a.amarela, jMes.jm order by pTemp desc ) r \n");
		sb.append("left join ( \n");
		sb.append("select sum(valor) valor, id_atleta, id_categoria from adicoes_deducoes where dt_ocorrencia >=:inicio and dt_ocorrencia <=:fim \n");
		sb.append("group by id_atleta, id_categoria) ad on ad.id_atleta = r.apont and ad.id_categoria = (select id_categoria from atleta where id = r.apont) \n");
		sb.append("order by r.pTri desc, (r.pTemp + ifnull(ad.valor,0)) desc,r.vTri desc, r.v desc, \n");
		
		sb.append("(case when ((r.vTri + r.dTri) = 0) then 0 else ((r.vTri /(r.vTri + r.dTri))*100) end)  desc, \n");
		sb.append("(case when ((r.v + r.d) = 0) then 0 else ((r.v / (r.v + r.d))*100) end) desc, \n");
		sb.append("r.nome asc \n");
		
		Session se = HibernateUtil.getSession();
		List<Rank> ranks = new ArrayList<Rank>();
		try {
			Period p = DateUtil.getCurrentDateOfMonth();
			Period temp = DateUtil.getCurrentDateOfYaer(); 
			Period trim = DateUtil.currentQuarter();
			SQLQuery q = se.createSQLQuery(sb.toString());
			// Setters
			q.setTimestamp("inicio", p.getStartDate());
			q.setTimestamp("fim", p.getEndDate());
			q.setTimestamp("dtIniTemp", temp.getStartDate());
			q.setTimestamp("dtFimTemp", temp.getEndDate());
			q.setTimestamp("iniTri", trim.getStartDate());
			q.setTimestamp("fimTri", trim.getEndDate());
			
			q.setLong("idCategoria", idCategoria);
			// Scalar
			q.addScalar("apont", StandardBasicTypes.LONG);		//0
			q.addScalar("nome", StandardBasicTypes.STRING);		//1
			q.addScalar("v", StandardBasicTypes.INTEGER);		//2
			q.addScalar("d", StandardBasicTypes.INTEGER);		//3
			q.addScalar("jm", StandardBasicTypes.INTEGER);		//4
			q.addScalar("j", StandardBasicTypes.INTEGER);		//5
			q.addScalar("pTemp", StandardBasicTypes.INTEGER);	//6
			q.addScalar("pMes", StandardBasicTypes.INTEGER);	//7
			q.addScalar("amarela", StandardBasicTypes.BOOLEAN);	//8
			q.addScalar("pTri", StandardBasicTypes.INTEGER);	//9
			// Novos
			q.addScalar("jTri", StandardBasicTypes.INTEGER);	//10
			q.addScalar("percTri", StandardBasicTypes.DOUBLE);	//11
			q.addScalar("percT", StandardBasicTypes.DOUBLE);	//12
			q.addScalar("vTri", StandardBasicTypes.INTEGER);	//13
			q.addScalar("dTri", StandardBasicTypes.INTEGER);	//14
			
			
			List<Object[]> objetos = q.list();
			int pos = 1;
			for (Object[] o : objetos) {
				Rank r = new Rank();
				r.setId((o[0] !=null ? ((Long)o[0]) : 0));
				r.setNome((o[1] !=null ? (o[1].toString()) : ""));
				r.setVitoria((o[2] !=null ? ((Integer)o[2]) : 0));
				r.setDerrota((o[3] !=null ? ((Integer)o[3]) : 0));
				r.setJMes((o[4] !=null ? ((Integer)o[4]) : 0));
				r.setJTemp((o[5] !=null ? ((Integer)o[5]) : 0));
				r.setPTemp((o[6] !=null ? ((Integer)o[6]) : 0));
				r.setPMes((o[7] !=null ? ((Integer)o[7]) : 0));
				r.setPosition(pos);
				r.setAmarela((o[8] !=null ? ((Boolean)o[8]) : false));
				r.setPTri((o[9] !=null ? ((Integer)o[9]) : 0));
				
				r.setJTri((o[10] !=null ? ((Integer)o[10]) : 0));
				r.setPercTri((o[11] !=null ? ((Double)o[11]) : 0d));
				r.setPercT((o[12] !=null ? ((Double)o[12]) : 0d));
				r.setVTri((o[13] !=null ? ((Integer)o[13]) : 0));
				r.setDTri((o[14] !=null ? ((Integer)o[14]) : 0));
				
				pos++;
				ranks.add(r);
			}
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		} finally {
			se.close();
		}
		//Collections.sort(ranks);
		return ranks;
	}
}
