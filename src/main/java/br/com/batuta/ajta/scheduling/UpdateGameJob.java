package br.com.batuta.ajta.scheduling;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component("updateGame")
public class UpdateGameJob {
	
	@PostConstruct
	public void updateGamesWithApprove() {
		System.out.println("Atualizando!!");
	}

}