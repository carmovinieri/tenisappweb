package br.com.batuta.ajta.security;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.service.AthleteService;

public class UserDetailsServiceImpl implements UserDetailsService{

	private final Logger log = Logger.getLogger(getClass());
	
	@Autowired
	private AthleteService athleteBusiness;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Athlete athlete = null;
		
		try {
			athlete = athleteBusiness.findByEmail(email);
		} catch (AjtaException e) {
			log.error(String.format("Erro ao tentar fazer login com o email %s", email));
			log.error(e.getLocalizedMessage(), e);
		}
		
		return athlete == null ? new Athlete() : athlete;
	}

}
