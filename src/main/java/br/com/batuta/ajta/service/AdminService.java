package br.com.batuta.ajta.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.entity.Game;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.exception.RepositoryException;
import br.com.batuta.ajta.exception.ServiceException;
import br.com.batuta.ajta.repository.AthleteRepository;
import br.com.batuta.ajta.repository.GameRepository;

@Service
public class AdminService {

	private final Logger log = Logger.getLogger(getClass());
	
	private AthleteRepository athleteRepository;
	
	private GameRepository gameRepository;
	
	@Autowired
	public AdminService(AthleteRepository athleteRepository, GameRepository gameRepository) {
		super();
		this.athleteRepository = athleteRepository;
		this.gameRepository = gameRepository;
	}

	public List<Athlete> list(Long idCategoria) {
		List<Athlete> athletes = new ArrayList<Athlete>();
		
		try {
			athletes = athleteRepository.list(idCategoria);
			
			Collections.sort(athletes);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		
		return athletes;
	}
	
	public List<Athlete> listPendingAthletes() {
		return athleteRepository.listPendingAthletes();
	}
	
	public List<Game> listPendingGames() throws AjtaException {
		try {
			return gameRepository.listPendingGames();
		} catch (RepositoryException e) {
			throw new AjtaException(e.getLocalizedMessage(), e);
		}
	}
	
	public void approveAthlete(Long athleteId) throws AjtaException {
		try {
			Athlete athlete = athleteRepository.getById(athleteId);
			athlete.setEnabled(true);
			
			athleteRepository.save(athlete);
		} catch (RepositoryException e) {
			e.printStackTrace();
			throw new AjtaException(e.getLocalizedMessage(), e);
		}
	}
	
	public void approveGame(Long gameId) throws AjtaException {
		try {
			Game game = gameRepository.findById(gameId);
			game.setStatusGame(Game.STATUS_APROVADO);
			
			gameRepository.save(game);
		} catch (RepositoryException e) {
			e.printStackTrace();
			throw new AjtaException(e.getLocalizedMessage(), e);
		}
	}
	
	public void repproveGame(Long gameId) throws AjtaException {
		try {
			Game game = gameRepository.findById(gameId);
			game.setStatusGame(Game.STATUS_REPROVADO);
			
			gameRepository.save(game);
		} catch (RepositoryException e) {
			e.printStackTrace();
			throw new AjtaException(e.getLocalizedMessage(), e);
		}
	}
	
	public void update(Athlete athlete) throws AjtaException {
		try {
			Athlete dbAthlete = athleteRepository.findById(athlete.getId());
			BeanUtils.copyProperties(athlete, dbAthlete, "dateOfCreation", "password", "rg");
			athleteRepository.save(dbAthlete);
		} catch(Exception e) {
			log.error("Erro durante a execução das regras de negócio");
			log.error(e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}
}
