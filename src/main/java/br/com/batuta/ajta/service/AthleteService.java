package br.com.batuta.ajta.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.entity.ForgotPasswordIdentifier;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.exception.RepositoryException;
import br.com.batuta.ajta.exception.ServiceException;
import br.com.batuta.ajta.pojo.BlockGamer;
import br.com.batuta.ajta.repository.AthleteRepository;
import br.com.batuta.ajta.repository.PasswordRepository;
import br.com.batuta.ajta.system.util.DateUtil;
import br.com.batuta.ajta.system.vo.Period;

@Service
public class AthleteService {
	
	private final Logger log = Logger.getLogger(getClass());
	
	@Autowired
	private AthleteRepository athleteRepository;
	
	@Autowired
	private PasswordRepository passwordRepository;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private MailService mailService;
	
	public void save(Athlete athlete) throws AjtaException {
		try {
			athlete.setDateOfCreation(new Date());
			athlete.setEnabled(false);
			athlete.setAdministrador(false);
			athlete.setPassword(encoder.encode(athlete.getPassword()));
			
			athleteRepository.save(athlete);
			try {
				mailService.sendSignUpMail(athlete);
			} catch (Exception ee) {
				log.error(ee.getLocalizedMessage(), ee);
			}
			try {
				mailService.sendSignUpMailAdministrator(athlete);
			} catch (Exception ee2) {
				log.error(ee2.getLocalizedMessage(), ee2);
			}
		} catch(Exception e) {
			log.error("Erro durante a execução das regras de negócio");
			log.error(e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}
	
	public void update(Athlete athlete) throws AjtaException {
		try {
			Athlete dbAthlete = athleteRepository.findById(athlete.getId());
			BeanUtils.copyProperties(athlete, dbAthlete, "category", "dateOfCreation", "camisaAmarela", "administrador", "systemAdm", "password", "enabled");
			athleteRepository.save(dbAthlete);
		} catch(Exception e) {
			log.error("Erro durante a execução das regras de negócio");
			log.error(e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}
	
	public void resetPassword(Athlete athlete, String resetPasswordId) throws AjtaException {
		try {
			ForgotPasswordIdentifier resetPassword = passwordRepository.findEnabledById(resetPasswordId);
			
			if (resetPassword == null) {
				throw new Exception("Link inválido ou expirado.");
			}
			
			Athlete currentAthlete = athleteRepository.findByEmail(athlete.getEmail());
			currentAthlete.setPassword(encoder.encode(athlete.getPassword()));
			athleteRepository.save(currentAthlete);
			passwordRepository.disableIds(athlete.getEmail());
		} catch(RepositoryException de) {
			throw de;
		} catch(Exception e) {
			log.error("Erro durante a execução das regras de negócio");
			log.error(e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}
	
	public List<Athlete> mobileList(Long idCategoria, Long idAtleta) {
		Athlete athlete = athleteRepository.findById(idAtleta);
		List<Athlete> atletas = athleteRepository.mobileList(idCategoria, idAtleta);
		//if (DateUtil.hasHitPlayer()) {
			try {
				hitPlayed(athleteRepository.findPlayersBlocked(DateUtil.mounthPast(), idCategoria, idAtleta), atletas, athlete);
			} catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
			}
		//}
		return atletas; 
	}
	
	public Athlete mobileLogin(Athlete athlete) throws AjtaException {
		Athlete databaseAthlete = null;
		
		try {
			databaseAthlete = athleteRepository.findByEmail(athlete.getEmail());
			
			if (databaseAthlete == null || !encoder.matches(athlete.getPassword(), databaseAthlete.getPassword()) || !databaseAthlete.isEnabled()) {
				return null;
			}
		} catch (RepositoryException de) {
			throw de;
		} catch (Exception e) {
			log.error("Erro durante a validação do login mobile.");
			log.error(e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
		
		
		return databaseAthlete;
	}
	
	public List<Athlete> list(Long idCategoria) {
		List<Athlete> athletes = new ArrayList<Athlete>();
		
		try {
			athletes = athleteRepository.list(idCategoria);
			
			Collections.sort(athletes);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		
		return athletes;
	}
	
	public Athlete findByEmail(String email) throws AjtaException {
		Athlete athlete = null;
		
		try {
			athlete = athleteRepository.findByEmail(email);
		} catch(RepositoryException de) {
			throw de;
		} catch(Exception e) {
			log.error("Erro durante a execução das regras de negócio");
			log.error(e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
		
		return athlete;
	}
	
	public Athlete findById(Long id) {
		return athleteRepository.findById(id);
	}
	
	public List<Athlete> inputGameWebList(Long idCategoria, Long idAtleta) {
		List<Athlete> athletesInput = athleteRepository.mobileList(idCategoria, idAtleta);
		List<Athlete> defer = new ArrayList<Athlete>();
		
		for (Athlete a : athletesInput) {
			if (!a.isJaJogou()) {
				defer.add(a);
			}
		}
		
		Collections.sort(defer);
		
		return defer;
	}

	public void approveStatus(Athlete atleta) throws Exception {
		athleteRepository.activeStatus(atleta);
		try {
			mailService.sendApproveSignUpMail(atleta);
		} catch (Exception ee2) {
			log.error(ee2.getLocalizedMessage(), ee2);
		}
	}
	
	public List<Athlete> listToApprove(Long idCategoria) {
		List<Athlete> athletes = new ArrayList<Athlete>();
		
		try {
			athletes = athleteRepository.listToApprove(idCategoria);
			
			Collections.sort(athletes);
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		
		return athletes;
	}
	
	public Boolean madeAllGamesOnMounth(Long idAthlete) {
		return athleteRepository.madeAllGamesOnMounth(idAthlete);
	}
	
	private void hitPlayed(List<BlockGamer> block, List<Athlete> players, Athlete athlete) {
		boolean blockAll = false;
		Period perSeasson = DateUtil.seassonPeriod();
		
		if (!athlete.isEnabled() || (perSeasson.getStartDate().after(DateUtil.getCurrentDateOfDay().getStartDate()) &&
				DateUtil.getCurrentDateOfDay().getEndDate().before(perSeasson.getEndDate()))) {
			blockAll = true;
		}
		
		for (Athlete a : players) {
			if (blockAll) {
				a.setJaJogou(true);
				continue;
			}
			if (a.getId() == 41) {
				System.out.println("Este!");
			}
			if (a.isJaJogou()) {
				continue;
			}
			if (block.contains(new BlockGamer(a))) {
				if (block.get(block.indexOf(new BlockGamer(a))).getQuantidade() >= 1) {
				//if (block.get(block.indexOf(new BlockGamer(a))).getQuantidade() >= 2) {
					a.setJaJogou(true);
				}
			}
		}
	}
}
