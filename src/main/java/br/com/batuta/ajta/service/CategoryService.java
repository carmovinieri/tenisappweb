package br.com.batuta.ajta.service;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.batuta.ajta.entity.Category;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.exception.ServiceException;
import br.com.batuta.ajta.exception.RepositoryException;
import br.com.batuta.ajta.repository.CategoryRepository;

@Service
public class CategoryService {

	private final Logger log = Logger.getLogger(getClass());
	
	@Autowired
	private CategoryRepository dao;
	
	public List<Category> listar() {
		return dao.listar();
	}
	
	public void save(Category category) throws AjtaException {
		try {
			category.setDataInclusao(new Date());
			category.setStatus(true);
			
			dao.save(category);
		} catch(RepositoryException de) {
			throw de;
		} catch(Exception e) {
			log.error("Erro durante a execução das regras de negócio");
			log.error(e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}
	
	public Category findById(Long idCategory) throws Exception {
		return dao.findById(idCategory);
	}
}
