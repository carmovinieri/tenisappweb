package br.com.batuta.ajta.service;

import java.util.List;

import br.com.batuta.ajta.repository.FeatureRepository;

public class FeatureService {

	public static void inserirFuncionalidades() throws Exception {
		new FeatureRepository().inserirFuncionalidades();
	}
	
	public static List<String> findCoordenators() throws Exception {
		return new FeatureRepository().findCoordinators();
	}
}
