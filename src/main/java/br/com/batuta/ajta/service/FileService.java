package br.com.batuta.ajta.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileService {

	private Logger log = Logger.getLogger(getClass());
	
	public static final String FILE_FOLDER = "C://APP-BETA";
	
	public static final String FILE_NAME = "ajta-app-beta.apk";
	
	public File retrieveApk() {
		File file = new File(FileService.FILE_FOLDER + File.separator + FileService.FILE_NAME);
		return file;
	}
	
	public void uploadApk(MultipartFile uploadFile) throws Exception {
		if (!uploadFile.getOriginalFilename().endsWith(".apk")) {
			throw new Exception();
		}
		
		try {
			byte[] bytes = uploadFile.getBytes();
			File folder = new File(FileService.FILE_FOLDER);
			
			if (!folder.isDirectory()) {
				folder.mkdirs();
			}
			
			File file = new File(folder.getPath() + File.separator + FileService.FILE_NAME);
			
			if (file.exists()) {
				file.delete();
			}
			
			OutputStream os = new FileOutputStream(file);
			BufferedOutputStream stream = new BufferedOutputStream(os);
			
			try {
				stream.write(bytes);
			} finally {
				stream.close();
			}
		} catch (IOException e) {
			log.error("Erro ao fazer upload do APK");
			log.error(e.getLocalizedMessage(), e);
			throw e;
		}
	}
}
