package br.com.batuta.ajta.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.batuta.ajta.entity.Game;
import br.com.batuta.ajta.exception.AfterDateOfServerException;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.exception.AthleteDesactiveException;
import br.com.batuta.ajta.exception.GameInMounthPastException;
import br.com.batuta.ajta.exception.MoreThanOneGameException;
import br.com.batuta.ajta.exception.RepositoryException;
import br.com.batuta.ajta.exception.ScoreValidationException;
import br.com.batuta.ajta.exception.ServiceException;
import br.com.batuta.ajta.repository.AthleteRepository;
import br.com.batuta.ajta.repository.GameRepository;
import br.com.batuta.ajta.system.util.DateUtil;
import br.com.batuta.ajta.system.util.GameValidateUtil;
import br.com.batuta.ajta.system.util.ImageUtil;
import br.com.batuta.ajta.system.vo.Period;

@Service
public class GameService {

	private final Logger log = Logger.getLogger(getClass());
	
	private GameRepository gameRepository;
	
	private AthleteRepository athleteRepository;
	
	@Autowired
	public GameService(GameRepository gameRepository, AthleteRepository athleteRepository) {
		super();
		this.gameRepository = gameRepository;
		this.athleteRepository = athleteRepository;
	}

	public Game saveMobile(Game game) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR_OF_DAY, -3);
		game.setDataInclusao(c.getTime());
		game.setIdSeqJogo(game.getDataJogo().getTime());
		
		try {
			setYellowShirt(game);
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		
		return save(game);
	}

	public Game saveWeb(Game game) {
		if (game.getDataJogo() == null) {
			game.setDataJogo(new Date());
		}
		
		game.setDataInclusao(new Date());
		game.setIdSeqJogo(game.getDataInclusao().getTime());
		
		try {
			setYellowShirt(game);
			game.setFoto(ImageUtil.resizeImage(game.getFoto()));
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		
		return save(game);
	}
	
	private void setYellowShirt(Game game) throws RepositoryException {
		
		Integer sharpenerYellow = athleteRepository.isPlayerYellowShirt(game.getApontador().getId());
		Integer opponentYellow = athleteRepository.isPlayerYellowShirt(game.getAdversario().getId());
		
		if (sharpenerYellow.equals(1) && opponentYellow.equals(1)) {
			game.setAmareloApontador(1);
			game.setAmareloAdversario(1);
		} else if (sharpenerYellow.equals(1) && opponentYellow.equals(0)) {
			game.setAmareloApontador(0);
			game.setAmareloAdversario(1);
		} else if (sharpenerYellow.equals(0) && opponentYellow.equals(1)) {
			game.setAmareloApontador(1);
			game.setAmareloAdversario(0);
		} else {
			game.setAmareloApontador(0);
			game.setAmareloAdversario(0);
		}
		
	}
	
	private Game save(Game game) {
		Game existingGame = gameRepository.loadBySeqAndAtleta(game.getIdSeqJogo(), game.getApontador().getId());
		
		if (existingGame == null) {
			existingGame = game;
			try {
				validateGame(existingGame);
			} catch (MoreThanOneGameException mtoge) {
				log.warn(mtoge.getMessage());
				existingGame.setStatusGame(Game.STATUS_MORTHANONEGAME);
			} catch (ScoreValidationException sve) {
				log.warn(sve.getMessage());
				existingGame.setStatusGame(Game.STATUS_INVALIDSCORE);
			} catch (AfterDateOfServerException adse) {
				log.warn(adse.getMessage());
				existingGame.setStatusGame(Game.STATUS_DATASUPERIOR);
			} catch (AthleteDesactiveException ade) {
				log.warn(ade.getMessage());
				existingGame.setStatusGame(Game.STATUS_ATHLETE_DESATIVADO);
			} catch (GameInMounthPastException gipe) {
				log.warn(gipe.getMessage());
				existingGame.setStatusGame(Game.STATUS_GAME_OUT_MOUNTH);
			} catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				existingGame.setStatusGame(Game.STATUS_GENERIC);
			}
		} else {
			existingGame.setAdversario(game.getAdversario());
			existingGame.setApontador(game.getApontador());
			existingGame.setFoto(game.getFoto());
			existingGame.setGamesCon(game.getGamesCon());
			existingGame.setGamesPro(game.getGamesPro());
		}
		
		
		return gameRepository.save(existingGame);
	}
	
	public List<Game> list(Date startDate, Date endDate, Long categoryId, Long athleteId, Integer statusGame) {
		if (endDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(endDate);
			cal.set(Calendar.AM_PM, Calendar.PM);
			cal.set(Calendar.HOUR, 11);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			
			endDate = cal.getTime();
		}
		
		return gameRepository.list(startDate, endDate, categoryId, athleteId, statusGame);
	}
	
	public List<Game> listUserGames(Long athleteId) {
		Period p = DateUtil.getCurrentDateOfMonth();
		
		return gameRepository.list(p.getStartDate(), p.getEndDate(), null, athleteId, null);
	}
	
	public List<Game> listUserGames(Long athleteId, Integer status) {
		Period p = DateUtil.getCurrentDateOfMonth();
		
		return gameRepository.list(p.getStartDate(), p.getEndDate(), null, athleteId, status);
	}
	
	public List<Game> listToMobile(Integer period, Long categoryId, Long athleteId) {
		
		Period p = DateUtil.getCurrentDateOfMonth();
		
		List<Game> listMob = gameRepository.list(p.getStartDate(), p.getEndDate(), categoryId, athleteId, null);
				
		return listMob;
	}
	
	public List<Game> listToAproveMobile(Long athleteId) {
		return gameRepository.listToAproveMobile(athleteId);
	}

	public void approveGame(Game game) throws AjtaException {
		try {
			gameRepository.approveGame(game);
		} catch(RepositoryException de) {
			throw new AjtaException(de.getLocalizedMessage(), de);
		} catch(Exception e) {
			log.error(String.format("Erro durante a execução de regras de negócio do jogo %s", game.getId()));
			log.error(e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}
	
	private void validateGame(Game game) throws Exception {
		if (!GameValidateUtil.validateScore(game)) {
			throw new ScoreValidationException("Placar digitado invalido!!<br/>Por favor digite um placar valido!!");
		}
		
		if (GameValidateUtil.validateHasGameInMounthPast(game)) {
			throw new GameInMounthPastException("Atletas já fizeram jogo mes passado!!");
		}
		
		if (GameValidateUtil.validateHasGameInMounth(game)) {
			throw new MoreThanOneGameException("Atletas já fizeram jogo nesse mês!!");
		}
		
		if (GameValidateUtil.validateActiveAthlete(game)) {
			throw new AthleteDesactiveException("Atletas desativados!!");
		}
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, 20);
		
		if (game.getDataJogo().after(c.getTime())) {
			game.setDataJogo(game.getDataInclusao());
			//Alteração feita devido a mudanças de data hora feita pelo atleta.
			//throw new AfterDateOfServerException("Jogo enviado com a data maior que a tolerancia de 20 minutos!!");
		}
	}
}
