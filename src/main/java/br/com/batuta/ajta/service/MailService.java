package br.com.batuta.ajta.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.entity.ForgotPasswordIdentifier;

@Service
public class MailService {

	private final Logger log = Logger.getLogger(getClass());
	
	@Autowired
	private MailSender mailSender;
	
	public static List<String> coordenadores = new ArrayList<String>();
	
	public void sendSignUpMail(Athlete athlete) {
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(athlete.getEmail());
		mail.setSubject("Bem vindo ao Portal AJTA!");
		mail.setText(String.format("Bem vindo, %s\nSua inscrição foi feita, porém ela ainda precisar ser aprovada por nossos "
				+ "administradores.\nAssim que a aprovação ocorrer você será notificado via e-mail.", athlete.getName()));
		
		try {
			mailSender.send(mail);
		} catch(MailSendException me) {
			log.error("Erro ao enviar o email de confirmação de cadastro de atleta");
			log.error(me.getLocalizedMessage(), me);
		}
	}
	
	public void sendSignUpMailAdministrator(Athlete athlete) {
		SimpleMailMessage mail = new SimpleMailMessage();
		
		List<String> emails = new ArrayList<String>();
		emails.addAll(coordenadores);
		String[] arr = new String[emails.size()];
		emails.toArray(arr);
		
		mail.setTo(arr);
		mail.setSubject("Atleta cadastrado no Portal AJTA!");
		mail.setText(String.format("Atleta cadastrado!\n Nome: %s \n Categoria: %s\n Inscrição foi feita pendente de aprovação! ", athlete.getName(), athlete.getCategory().getDescricao()));
		
		try {
			mailSender.send(mail);
		} catch(MailSendException me) {
			log.error("Erro ao enviar o email de confirmação de cadastro de atleta");
			log.error(me.getLocalizedMessage(), me);
		}
	}

	public void sendForgotPasswordMail(ForgotPasswordIdentifier forgotPasswordIdentifier) {
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(forgotPasswordIdentifier.getEmail());
		mail.setSubject("Recupere sua senha!");
		mail.setText(String.format("Recupere a senha: http://ajta.tplayweb.com/AJTAPLAY/app/#/reset-password?id=%s", 
				forgotPasswordIdentifier.getId()));
		
		try {
			mailSender.send(mail);
		} catch(MailSendException me) {
			log.error("Erro ao enviar o email de reset de senha");
			log.error(me.getLocalizedMessage(), me);
		}
	}
	
	public void sendApproveSignUpMail(Athlete athlete) {
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(athlete.getEmail());
		mail.setSubject("Inscrição aprovada!");
		mail.setText(String.format("%s ,\nSua inscrição foi aprovada com exito. Você já pode utilizar o aplicativo e iniciar seus jogos. ", athlete.getName()));
		
		try {
			mailSender.send(mail);
		} catch(MailSendException me) {
			log.error("Erro ao enviar o email de confirmação de cadastro de atleta");
			log.error(me.getLocalizedMessage(), me);
		}
	}
}
