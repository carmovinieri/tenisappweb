package br.com.batuta.ajta.service;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.batuta.ajta.entity.ForgotPasswordIdentifier;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.exception.ServiceException;
import br.com.batuta.ajta.exception.RepositoryException;
import br.com.batuta.ajta.repository.AthleteRepository;
import br.com.batuta.ajta.repository.PasswordRepository;

@Service
public class PasswordService {
	
	private final Logger log = Logger.getLogger(getClass());
	
	private PasswordRepository passwordRepository;
	
	private AthleteRepository athleteRepository;
	
	private MailService mailService;
	
	@Autowired
	public PasswordService(PasswordRepository passwordRepository, AthleteRepository athleteRepository,
			MailService mailService) {
		super();
		this.passwordRepository = passwordRepository;
		this.athleteRepository = athleteRepository;
		this.mailService = mailService;
	}
	
	public void sendMail(ForgotPasswordIdentifier forgotPasswordIdentifier) throws AjtaException {
		try {
			if (athleteRepository.findByEmail(forgotPasswordIdentifier.getEmail()) != null) {
				forgotPasswordIdentifier.setDateOfCreation(new Date());
				forgotPasswordIdentifier.setId(UUID.randomUUID().toString());
				forgotPasswordIdentifier.setEnabled(true);
				
				passwordRepository.disableIds(forgotPasswordIdentifier.getEmail());
				passwordRepository.save(forgotPasswordIdentifier);
				mailService.sendForgotPasswordMail(forgotPasswordIdentifier);
			}
		} catch (RepositoryException de) {
			throw de;
		} catch (Exception e) {
			log.error("Erro durante a execução das regras de negócio.");
			log.error(e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}

	public ForgotPasswordIdentifier getForgotPasswordIdentifier(String id) throws AjtaException {
		ForgotPasswordIdentifier forgotPasswordIdentifier = passwordRepository.findEnabledById(id);
		
		if (forgotPasswordIdentifier != null) {
			Date currentDate = new Date();
			long timeDif = currentDate.getTime() - forgotPasswordIdentifier.getDateOfCreation().getTime();
			
			if (TimeUnit.MILLISECONDS.toHours(timeDif) < 24) {
				return forgotPasswordIdentifier;
			}
		}
		
		throw new ServiceException();
	}
}
