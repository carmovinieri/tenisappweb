package br.com.batuta.ajta.system.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import br.com.batuta.ajta.service.FeatureService;
import br.com.batuta.ajta.service.MailService;

@WebListener
public class ContextListener implements ServletContextListener {

	private Logger log = Logger.getLogger(getClass());
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		log.info("AJTA PLAY Encerrado!");
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		log.info("AJTA PLAY Inicializando ...");
		log.info("Atualizando funcionalidades...");
		try {
			FeatureService.inserirFuncionalidades();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		log.info("... Funcionalidades Atualizadas!");
		
		log.info("Atualizando funcionalidades...");
		
		try {
			MailService.coordenadores.addAll(FeatureService.findCoordenators());
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		log.info("Atualizando funcionalidades...");
		
		log.info("... AJTA PLAY Inicializado!");
	}

}
