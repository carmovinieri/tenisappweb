package br.com.batuta.ajta.system.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.batuta.ajta.system.vo.Period;

/**
 * Classe DateUtil
 * 
 * Responsavel por calcular as datas que o sistema ir� utilizar
 * 
 * @author carmo.vinieri
 *
 */
public class DateUtil {

	public static void main(String[] args) {
		try {
			
			Period p = getCurrentDateOfMonth();
			System.out.println("Mes:");
			System.out.println(p.getStartDate() + " - " + p.getEndDate());
			Period temp = getCurrentDateOfYaer(); 
			System.out.println("Temporada:");
			System.out.println(temp.getStartDate() + " - " + temp.getEndDate());
			Period trim = currentQuarter();
			System.out.println("Trimestre Corrente:");
			System.out.println(trim.getStartDate() + " - " + trim.getEndDate());
			Period um = firstQuarter();
			System.out.println(um.getStartDate() + " - " + um.getEndDate());
			Period dois = secondQuarter();
			System.out.println(dois.getStartDate() + " - " + dois.getEndDate());
			Period tres = thirdQuarter();
			System.out.println(tres.getStartDate() + " - " + tres.getEndDate());
			
			Period mesPassado = mounthPast();
			System.out.println(mesPassado.getStartDate() + " - " + mesPassado.getEndDate());
			
			Period periodo = seassonPeriod();
			System.out.println(periodo.getStartDate() + " - " + periodo.getEndDate());
			
//			Period p = mountPeriodFromMonth(DateUtil.convertDate("2016-02-21 22:15:34","yyyy-MM-dd HH:mm:ss"));//getCurrentDateOfDay();
//			System.out.println(p.getStartDate() + " - " + p.getEndDate());
			//System.out.println(DateUtil.convertDate("2015-11-21 22:15:34","yyyy-MM-dd HH:mm:ss").getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Metodo getCurrentDateOfMonth
	 * 
	 * Calcula o primeiro e o ultimo dia do mes corrente.
	 * 
	 * @return Instancia de Objeto Period
	 * 
	 */
	public static Period getCurrentDateOfMonth() {
		Period p = new Period();
		Calendar d = Calendar.getInstance();
		d.add(Calendar.HOUR_OF_DAY, -3);
		// Setando o primeiro dia do mes
		d.set(Calendar.DAY_OF_MONTH, 1);
		d.set(Calendar.HOUR_OF_DAY, 0);
		d.set(Calendar.MINUTE, 0);
		d.set(Calendar.SECOND, 0);
		p.setStartDate(d.getTime());
		// Setando o ultimo dia do mes
		d.add(Calendar.MONTH, 1);
		d.add(Calendar.DAY_OF_MONTH, -1);
		d.set(Calendar.HOUR_OF_DAY, 23);
		d.set(Calendar.MINUTE, 59);
		d.set(Calendar.SECOND, 59);
		p.setEndDate(d.getTime());
		
		return p;
	}
	
	/**
	 * Metodo getCurrentDateOfYaer
	 * 
	 * Calcula o primeiro e o ultimo dia do ano corrente.
	 * 
	 * @return Instancia de Objeto Period
	 */
	public static Period getCurrentDateOfYaer() {
		Period p = new Period();
		Calendar d = Calendar.getInstance();
		d.add(Calendar.HOUR_OF_DAY, -3);
		// Setando o primeiro dia do ano
		d.set(Calendar.DAY_OF_MONTH, 1);
		d.set(Calendar.MONTH, Calendar.JANUARY);
		d.set(Calendar.HOUR_OF_DAY, 0);
		d.set(Calendar.MINUTE, 0);
		d.set(Calendar.SECOND, 0);
		p.setStartDate(d.getTime());
		// Setando o ultimo dia do ano
		d.set(Calendar.DAY_OF_MONTH, 31);
		d.set(Calendar.MONTH, Calendar.DECEMBER);
		d.set(Calendar.HOUR_OF_DAY, 23);
		d.set(Calendar.MINUTE, 59);
		d.set(Calendar.SECOND, 59);
		p.setEndDate(d.getTime());
		return p;
	}
	
	public static Period getCurrentDateOfDay() {
		Period p = new Period();
		Calendar d = Calendar.getInstance();
		d.add(Calendar.HOUR_OF_DAY, -3);
		// Setando o primeiro dia do ano
		d.set(Calendar.HOUR_OF_DAY, 0);
		d.set(Calendar.MINUTE, 0);
		d.set(Calendar.SECOND, 0);
		p.setStartDate(d.getTime());
		// Setando o ultimo dia do ano
		d.set(Calendar.HOUR_OF_DAY, 23);
		d.set(Calendar.MINUTE, 59);
		d.set(Calendar.SECOND, 59);
		p.setEndDate(d.getTime());
		return p;
	}
	
	/**
	 * Converte String do formato especificado para Date. Se o formato passado
	 * pelo par�metro for nulo, retorna no formato dd/MM/yyyy.
	 * 
	 * @param String
	 *            date - Data no formato String
	 * @param String
	 *            format - Formato da data
	 * @return Date - Data convertida do formato desejado
	 * @throws ParseException
	 */
	public static Date convertDate(String date, String format)
			throws ParseException {
		if (date != null) {
			SimpleDateFormat sdf = format == null ? new SimpleDateFormat(
					"dd/MM/yyyy") : new SimpleDateFormat(format);
			return sdf.parse(date);
		} else {
			return null;
		}
	}
	
	public static String convertDate(Date date, String format)
			throws ParseException {
		if (date != null) {
			SimpleDateFormat sdf = format == null ? new SimpleDateFormat(
					"dd/MM/yyyy HH:mm:ss") : new SimpleDateFormat(format);
			return sdf.format(date);
		} else {
			return null;
		}
	}
	
	public static Period mountPeriodFromMonth(Date dtJogo){
		Period p = new Period();
		Calendar d = Calendar.getInstance();
		d.setTime(dtJogo);
		// Setando o primeiro dia do mes
		d.set(Calendar.DAY_OF_MONTH, 1);
		d.set(Calendar.HOUR_OF_DAY, 0);
		d.set(Calendar.MINUTE, 0);
		d.set(Calendar.SECOND, 0);
		p.setStartDate(d.getTime());
		// Setando o ultimo dia do mes
		d.add(Calendar.MONTH, 1);
		d.add(Calendar.DAY_OF_MONTH, -1);
		d.set(Calendar.HOUR_OF_DAY, 23);
		d.set(Calendar.MINUTE, 59);
		d.set(Calendar.SECOND, 59);
		p.setEndDate(d.getTime());
		
		return p;
	}
	
	public static boolean hasHitPlayer() {
		Calendar d = Calendar.getInstance();
		Integer month = d.get(Calendar.MONTH);
		if (month.equals(Calendar.MARCH) || month.equals(Calendar.JUNE) || month.equals(Calendar.SEPTEMBER)) {
			return true;
		}
		return false;
	}
	
	public static Period currentQuarter() {
		Period p = new Period();
		Calendar d = Calendar.getInstance();
		d.add(Calendar.HOUR_OF_DAY, -3);
		Integer mounth = d.get(Calendar.MONTH);
		//if (mounth.equals(Calendar.JANUARY) || mounth.equals(Calendar.FEBRUARY) || mounth.equals(Calendar.MARCH)) {
		if (mounth.equals(Calendar.MARCH) || mounth.equals(Calendar.APRIL) || mounth.equals(Calendar.MAY)) {
			p = firstQuarter();
		//} else if (mounth.equals(Calendar.APRIL) || mounth.equals(Calendar.MAY) || mounth.equals(Calendar.JUNE)) {
		} else if (mounth.equals(Calendar.JUNE) || mounth.equals(Calendar.JULY) || mounth.equals(Calendar.AUGUST)) {
			p = secondQuarter();
		//} else if (mounth.equals(Calendar.JULY) || mounth.equals(Calendar.AUGUST) || mounth.equals(Calendar.SEPTEMBER)) {
		} else if (mounth.equals(Calendar.SEPTEMBER) || mounth.equals(Calendar.OCTOBER) || mounth.equals(Calendar.NOVEMBER)) {
			p = thirdQuarter();
		}
		return p;
	}
	
	private static Period firstQuarter() {
		Period p = new Period();
		Calendar d = Calendar.getInstance();
		d.add(Calendar.HOUR_OF_DAY, -3);
		// Setando o primeiro dia do mes
		d.set(Calendar.DAY_OF_MONTH, 1);
		d.set(Calendar.HOUR_OF_DAY, 0);
		d.set(Calendar.MINUTE, 0);
		d.set(Calendar.SECOND, 0);
		//d.set(Calendar.MONTH, 0);
		d.set(Calendar.MONTH, 2);
		p.setStartDate(d.getTime());
		// Setando o ultimo dia do mes
		//d.add(Calendar.MONTH, 3);
		d.add(Calendar.MONTH, 5);
		d.add(Calendar.DAY_OF_MONTH, -1);
		d.set(Calendar.HOUR_OF_DAY, 23);
		d.set(Calendar.MINUTE, 59);
		d.set(Calendar.SECOND, 59);
		p.setEndDate(d.getTime());
		
		return p;
	}
	
	private static Period secondQuarter() {
		Period p = new Period();
		Calendar d = Calendar.getInstance();
		d.add(Calendar.HOUR_OF_DAY, -3);
		// Setando o primeiro dia do mes
		d.set(Calendar.DAY_OF_MONTH, 1);
		d.set(Calendar.HOUR_OF_DAY, 0);
		d.set(Calendar.MINUTE, 0);
		d.set(Calendar.SECOND, 0);
		//d.set(Calendar.MONTH, 3);
		d.set(Calendar.MONTH, 5);
		p.setStartDate(d.getTime());
		// Setando o ultimo dia do mes
		//d.add(Calendar.MONTH, 3);
		d.add(Calendar.MONTH, 8);
		d.add(Calendar.DAY_OF_MONTH, -1);
		d.set(Calendar.HOUR_OF_DAY, 23);
		d.set(Calendar.MINUTE, 59);
		d.set(Calendar.SECOND, 59);
		p.setEndDate(d.getTime());
		
		return p;
	}
	
	private static Period thirdQuarter() {
		Period p = new Period();
		Calendar d = Calendar.getInstance();
		d.add(Calendar.HOUR_OF_DAY, -3);
		// Setando o primeiro dia do mes
		d.set(Calendar.DAY_OF_MONTH, 1);
		d.set(Calendar.HOUR_OF_DAY, 0);
		d.set(Calendar.MINUTE, 0);
		d.set(Calendar.SECOND, 0);
		//d.set(Calendar.MONTH, 6);
		d.set(Calendar.MONTH, 8);
		p.setStartDate(d.getTime());
		// Setando o ultimo dia do mes
		//d.add(Calendar.MONTH, 3);
		d.add(Calendar.MONTH, 11);
		d.add(Calendar.DAY_OF_MONTH, -1);
		d.set(Calendar.HOUR_OF_DAY, 23);
		d.set(Calendar.MINUTE, 59);
		d.set(Calendar.SECOND, 59);
		p.setEndDate(d.getTime());
		
		return p;
	}
	
	public static Period mounthPast() {
		Period p = new Period();
		Calendar d = Calendar.getInstance();
		d.add(Calendar.HOUR_OF_DAY, -3);
		// Setando o primeiro dia do mes
		d.set(Calendar.DAY_OF_MONTH, 1);
		d.set(Calendar.HOUR_OF_DAY, 0);
		d.set(Calendar.MINUTE, 0);
		d.set(Calendar.SECOND, 0);
		d.add(Calendar.MONTH, -1);
		p.setStartDate(d.getTime());
		// Setando o ultimo dia do mes
		d.add(Calendar.MONTH, 1);
		d.add(Calendar.DAY_OF_MONTH, -1);
		d.set(Calendar.HOUR_OF_DAY, 23);
		d.set(Calendar.MINUTE, 59);
		d.set(Calendar.SECOND, 59);
		p.setEndDate(d.getTime());
		
		return p;
	}
	
	public static Period seassonPeriod() {
		Period p = new Period();
		Calendar d = Calendar.getInstance();
		d.add(Calendar.HOUR_OF_DAY, -3);
		// Setando o primeiro dia do mes
		d.set(Calendar.MONTH, Calendar.MARCH);
		d.set(Calendar.DAY_OF_MONTH, 1);
		d.set(Calendar.HOUR_OF_DAY, 0);
		d.set(Calendar.MINUTE, 0);
		d.set(Calendar.SECOND, 0);
		p.setStartDate(d.getTime());
		// Setando o ultimo dia do mes
		d.set(Calendar.MONTH, Calendar.NOVEMBER);
		d.add(Calendar.DAY_OF_MONTH, -1);
		d.set(Calendar.HOUR_OF_DAY, 23);
		d.set(Calendar.MINUTE, 59);
		d.set(Calendar.SECOND, 59);
		p.setEndDate(d.getTime());
		
		return p;
	}
}
