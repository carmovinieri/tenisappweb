package br.com.batuta.ajta.system.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.type.StandardBasicTypes;

import br.com.batuta.ajta.entity.Game;
import br.com.batuta.ajta.system.vo.Period;

public class GameValidateUtil {

	public static boolean validateScore(Game game) {
		boolean isValid = true;
		
		if (game.getGamesPro() > 7 || game.getGamesCon() > 7) {
			isValid = false;
		}
		
		if (game.getGamesPro() <= 5 && game.getGamesCon() <= 5) {
			isValid = false;
		}

		if (game.getGamesPro() == 6) {
			if (game.getGamesCon() >= 5 && game.getGamesCon() < 7) {
				isValid = false;
			}
		}

		if (game.getGamesCon() == 6) {
			if (game.getGamesPro() >= 5 && game.getGamesPro() < 7) {
				isValid = false;
			}
		}

		if (game.getGamesPro() == 7) {
			if (!(game.getGamesCon() >= 5 && game.getGamesCon() < 7)) {
				isValid = false;
			}
		}

		if (game.getGamesCon() == 7) {
			if (!(game.getGamesPro() >= 5 && game.getGamesPro() < 7)) {
				isValid = false;
			}
		}
		
		return isValid;
	}
	
	@SuppressWarnings("unchecked")
	public static boolean validateHasGameInMounth(Game game) throws Exception {
		StringBuffer sb = new StringBuffer("select distinct id from jogo where status_game not in (2,3,5) and dt_jogo >=:startDate and dt_jogo <=:endDate \n");
		sb.append("and id_apontador in (:players) and id_adversario in (:players) and id_categoria =:idCategoria \n");
		
		Session se = HibernateUtil.getSession();
		
		try {
			Period p = DateUtil.mountPeriodFromMonth(game.getDataJogo());
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			query.setTimestamp("startDate", p.getStartDate());
			query.setTimestamp("endDate", p.getEndDate());
			query.setLong("idCategoria", game.getCategoria().getId());
			List<Long> atletasId = new ArrayList<Long>();
			atletasId.add(game.getApontador().getId());
			atletasId.add(game.getAdversario().getId());
			query.setParameterList("players", atletasId);
			// Scalar
			query.addScalar("id", StandardBasicTypes.LONG);
			
			List<Object[]> objetos = query.list();
			
			if (objetos != null && objetos.size() > 0) {
				return true;
			}
			
			return false;
			
		} catch (Exception e) {
			throw e;
		} finally {
			se.close();
		}
	}
	
	public static boolean validateHasGameInMounthPast(Game game) throws Exception {
		StringBuffer sb = new StringBuffer("select distinct id from jogo where status_game not in (2,3,5) and dt_jogo >=:startDate and dt_jogo <=:endDate \n");
		sb.append("and id_apontador in (:players) and id_adversario in (:players) and id_categoria =:idCategoria \n");
		
		Session se = HibernateUtil.getSession();
		
		try {
			Calendar c = Calendar.getInstance();
			c.setTime(game.getDataJogo());
			c.add(Calendar.MONTH, -1);
			
			Period p = DateUtil.mountPeriodFromMonth(game.getDataJogo());
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			query.setTimestamp("startDate", p.getStartDate());
			query.setTimestamp("endDate", p.getEndDate());
			query.setLong("idCategoria", game.getCategoria().getId());
			List<Long> atletasId = new ArrayList<Long>();
			atletasId.add(game.getApontador().getId());
			atletasId.add(game.getAdversario().getId());
			query.setParameterList("players", atletasId);
			// Scalar
			query.addScalar("id", StandardBasicTypes.LONG);
			
			List<Object[]> objetos = query.list();
			
			if (objetos != null && objetos.size() > 0) {
				return true;
			}
			
			return false;
		} catch (Exception e) {
			throw e;
		} finally {
			se.close();
		}
	}
	
	public static boolean validateActiveAthlete(Game game) throws Exception {
		StringBuffer sb = new StringBuffer("select ativo from atleta where id =:idAtleta \n");
		
		Session se = HibernateUtil.getSession();
		
		try {
			SQLQuery query = se.createSQLQuery(sb.toString());
			// Setters
			query.setLong("idAtleta", game.getAdversario().getId());
			// Scalar
			query.addScalar("ativo", StandardBasicTypes.INTEGER);
			
			Integer validado = (Integer)query.uniqueResult();
			
			if (validado == null || validado.equals(new Integer(0))) {
				return true;
			}
			
			query = se.createSQLQuery(sb.toString());
			
			// Setters
			query.setLong("idAtleta", game.getApontador().getId());
			// Scalar
			query.addScalar("ativo", StandardBasicTypes.INTEGER);
			
			validado = (Integer)query.uniqueResult();
			
			if (validado == null || validado.equals(new Integer(0))) {
				return true;
			}
			
			return false;
			
		} catch (Exception e) {
			throw e;
		} finally {
			se.close();
		}
	}
	
}
