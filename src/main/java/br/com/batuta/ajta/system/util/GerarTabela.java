package br.com.batuta.ajta.system.util;

import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.tool.hbm2ddl.SchemaExport;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.entity.Category;
import br.com.batuta.ajta.entity.Feature;
import br.com.batuta.ajta.entity.Game;

public class GerarTabela {

	/**
	 * Método principal
	 */
	public static void main(String[] args) {
		try {
			
			MetadataSources metadata = new MetadataSources(
				    new StandardServiceRegistryBuilder()
				        .applySetting("hibernate.dialect", "org.hibernate.dialect.MySQLDialect")
				        .build());

				metadata.addAnnotatedClass(Athlete.class);
				metadata.addAnnotatedClass(Category.class);
				metadata.addAnnotatedClass(Feature.class);
				metadata.addAnnotatedClass(Game.class);

				SchemaExport export = new SchemaExport(
				    (MetadataImplementor) metadata.buildMetadata(),
				   null
				);
				export.create(true, false);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
