package br.com.batuta.ajta.system.util;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtil {

	
	private static final SessionFactory sessionFactory;
	 
    static {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
//            Configuration configuration = new Configuration();
//            configuration.configure();
            //sessionFactory = configuration.configure().buildSessionFactory();
//            StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();// .buildServiceRegistry();        
//            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            
//            sessionFactory = new AnnotationConfiguration().configure("hibernate.cfg.xml").buildSessionFactory();
        	
        	StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
	        .configure( "hibernate.cfg.xml" )
	        .build();
        	
        	Metadata metadata = new MetadataSources( standardRegistry )
	        .getMetadataBuilder()
	        .build();
        	
        	sessionFactory = metadata.getSessionFactoryBuilder().build();
        }
        catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
 
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
 
    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }
	
    public static Session getSession() throws HibernateException {
		return sessionFactory.openSession();
	}
}
