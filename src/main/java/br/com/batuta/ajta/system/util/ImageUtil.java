package br.com.batuta.ajta.system.util;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.imageio.ImageIO;

public class ImageUtil {

	public static byte[] resizeImage(byte[] realImage) throws Exception {
		if (realImage == null) {
			return null;
		}
		try {
			
			BufferedImage img = ImageIO.read(new ByteArrayInputStream(realImage));
			Image novaImage = img.getScaledInstance(600, 400, Image.SCALE_SMOOTH);
			
			ByteArrayOutputStream bos = null;
		    try {
		        bos = new ByteArrayOutputStream();
		        ImageIO.write(imageToBufferedImage(novaImage), "jpg", bos);
		    } finally {
		        try {
		            bos.close();
		        } catch (Exception e) {
		        }
		    }

		    return bos.toByteArray();
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	 public static BufferedImage imageToBufferedImage(Image im) {
	     BufferedImage bi = new BufferedImage(im.getWidth(null),im.getHeight(null),BufferedImage.TYPE_INT_RGB);
	     Graphics bg = bi.getGraphics();
	     bg.drawImage(im, 0, 0, null);
	     bg.dispose();
	     return bi;
	  }
}
