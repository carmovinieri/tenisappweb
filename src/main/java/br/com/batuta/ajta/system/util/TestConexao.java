package br.com.batuta.ajta.system.util;

import java.util.Date;

import org.hibernate.Session;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.entity.Game;

public class TestConexao {

	public static void main(String[] args) {
		
        Session session = HibernateUtil.getSession();
        
        Game j = new Game();
        
        Athlete a = new Athlete();
        a.setId(1l);
        Athlete b = new Athlete();
        b.setId(2l);
        
        j.setApontador(a);
        j.setAdversario(b);
        j.setDataInclusao(new Date());
        j.setDataJogo(new Date());
        j.setGamesCon(1);
        j.setIdSeqJogo(new Date().getTime());
        j.setGamesPro(6);
        j.setMensagem("Jogo Tetste");
        
        session.beginTransaction();
        session.saveOrUpdate(j);
        session.getTransaction().commit();
        
        session.close();
	}
}
