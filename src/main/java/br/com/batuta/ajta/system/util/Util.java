package br.com.batuta.ajta.system.util;

import java.text.ParseException;
import java.util.Calendar;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.batuta.ajta.system.vo.Period;

public class Util {

	public static void main(String[] args) {
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR_OF_DAY, -3);
		
		try {
			System.out.println(DateUtil.convertDate(c.getTime(), "dd/MM/yyyy HH:mm:ss"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		System.out.println(encoder.encode("123"));
		//$2a$10$Qp3XwoVPz8cyxF4hgd0D2ODsD2px/qLCa9IKaDr8EY2za5JMieEhq
		
		//$2a$10$E6GipAU0AVa.hs4RCNf5ieKsV4.fs1x10W6Nx/q9a3ylsdznd0gAC
		//System.out.println(encoder.matches("sony2005", "$2a$10$usEIOAgeXSKYP4/QpdKadOu28nCCnVgxklm6VYJdylnbJ5.lLAYci"));
		
		boolean blockAll = false;
		Period perSeasson = DateUtil.seassonPeriod();
		System.out.println(perSeasson.getStartDate()+" - "+perSeasson.getEndDate());
		System.out.println(DateUtil.getCurrentDateOfDay().getStartDate()+" - "+DateUtil.getCurrentDateOfDay().getEndDate());
		if (perSeasson.getStartDate().after(DateUtil.getCurrentDateOfDay().getStartDate()) &&
				DateUtil.getCurrentDateOfDay().getEndDate().before(perSeasson.getEndDate())) {
			blockAll = true;
		}
		
		System.out.println(blockAll);
		
	}
}
