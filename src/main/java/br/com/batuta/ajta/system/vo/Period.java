package br.com.batuta.ajta.system.vo;

import java.util.Date;

public class Period {

	private Date startDate;
	private Date endDate;
	
	public Period() {
		
	}
	
	public Period(Date inicio, Date fim) {
		this.startDate = inicio;
		this.endDate = fim;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
