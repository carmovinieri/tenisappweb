angular.module('tenis.controllers', []);
angular.module('tenis.services', []);
var app =angular.module('tenis.app', ['tenis.controllers', 'tenis.services', 'ui.router', 'blockUI', 'ui.bootstrap', 'naif.base64']);

app.config(function($stateProvider, $urlRouterProvider, blockUIConfig, $httpProvider) {
	blockUIConfig.message = 'Por favor, aguarde...';
	
	$stateProvider
	
	.state('app', {
		abstract: true,
		views: {
			'header': {
				templateUrl: 'template/header.html',
				controller: 'MainCtrl'
			},
			'content': {}
		}
	})
	
	//Public pages
	.state('app.home', {
		url: '/home',
		views: {
			'content@': {
				templateUrl: 'template/home.html'
			}
		}
	})
	
	.state('app.ranking', {
		url: '/ranking/:categoryId',
		views: {
			'content@': {
				templateUrl: 'template/ranking/ranking.html',
				controller: 'RankingCtrl'
			}
		}
	})
	
	.state('app.sign-up', {
		url: '/sign-up',
		views: {
			'content@': {
				templateUrl: 'template/sign-up/sign-up.html',
				controller: 'SignUpCtrl'
			}
		}
	})
	
	.state('app.athlete-list', {
		url: '/athlete-list/:categoryId',
		views: {
			'content@': {
				templateUrl: 'template/athlete/athlete-list.html',
				controller: 'AthleteListCtrl'
			}
		}
	})
	
	.state('app.game-list', {
		url: '/game-list',
		views: {
			'content@': {
				templateUrl: 'template/game/game-list.html',
				controller: 'GameListCtrl'
			}
		}
	})
	
	.state('app.category-list', {
		url: '/category-list',
		views: {
			'content@': {
				templateUrl: 'template/category/category-list.html',
				controller: 'CategoryListCtrl'
			}
		}
	})

	.state('app.reset-password', {
		url: '/reset-password?id',
		views: {
			'content@': {
				templateUrl: 'template/reset-password.html',
				controller: 'ResetPasswordCtrl'
			}
		}
	})
	
	.state('app.regulation', {
		url: '/regulation',
		views: {
			'content@': {
				templateUrl: 'template/regulation/regulation.html'
			}
		}
	})
	
	// End of public pages
	
	// Adm pages
	.state('app.edit-athlete', {
		url:'/adm/athlete/{id}',
		views: {
			'content@': {
				templateUrl: 'template/athlete/edit-athlete.html',
				controller: 'EditAthleteCtrl'
			}
		}
	})
	
	
	.state('app.create-category', {
		url: '/adm/category',
		views: {
			'content@': {
				templateUrl: 'template/category/create-category.html',
				controller: 'CreateCategoryCtrl'
			}
		}
	})
	
	.state('app.file-upload', {
		url: '/adm/file-upload',
		views: {
			'content@': {
				templateUrl: 'template/file-upload.html'
			}
		}
	})
	
	.state('app.approve-athlete', {
		url: '/adm/approve-athlete',
		views: {
			'content@': {
				templateUrl: 'template/adm/approve-athlete.html',
				controller: 'AthleteApprovalManagerCtrl'
			}
		}
	})
	
	.state('app.athlete-manager', {
		url: '/adm/athleteManager/:categoryId/:userId',
		views: {
			'content@': {
				templateUrl: 'template/adm/athlete-manager.html',
				controller: 'AthleteManagerCtrl'
			}
		}
	})
	
	
	.state('app.game-approval-manager',{
		url: '/adm/game-approval-manager',
		views: {
			'content@': {
				templateUrl: 'template/adm/game-approval-manager.html',
				controller: 'GameApprovalManagerCtrl'
			}
		}
	})
	
	// Not implemented
	.state('app.game-manager',{
		url: '/adm/game-manager',
		views: {
			'content@': {
				templateUrl: 'template/adm/game-manager.html',
				controller: 'GameManagerCtrl'
			}
		}
	})
	
	// End of adm pages
	
// Nao utilizado?
//	.state('app.gameInput', {
//		url: '/gameInput',
//		views: {
//			'content@': {
//				templateUrl: 'template/game/game-input.html',
//				controller: 'GameInsertCtrl'
//			}
//		}
//	})

	// Logged user pages
	.state('app.create-game', {
		url: '/secure/create-game',
		views: {
			'content@': {
				templateUrl: 'template/game/create-game.html',
				controller: 'CreateGameCtrl'
			}
		}
	})
	
	.state('app.approve-game', {
		url: '/secure/approve-game',
		views: {
			'content@': {
				templateUrl: 'template/game/approve-game.html',
				controller: 'ApproveGameCtrl'
			}
		}
	})
	
	.state('app.control-panel', {
		url: '/secure/control-panel',
		views: {
			'content@': {
				templateUrl: 'template/control-panel/my-account.html',
				controller: 'ControlPanelCtrl'
			}
		}
	})
	
	.state('app.my-games', {
		url: '/secure/my-games',
		views: {
			'content@': {
				templateUrl: 'template/control-panel/my-games.html',
				controller: 'MyGamesCtrl'
			}
		}
	})
	
	.state('app.my-profile', {
		url:'/secure/my-profile',
		views: {
			'content@': {
				templateUrl: 'template/control-panel/my-profile.html',
				controller: 'MyProfileCtrl'
			}
		}
	})
	
	// End of logged user pages
	;
	
	$urlRouterProvider.otherwise('/home');
})

.run(function($rootScope, UserService, $state) {
	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
		if (toState.url.indexOf('secure') > -1) {
			UserService.getLoggedUser().then(function(user) {
				if (!user) {
					goHome(event);
				}
			});
		}
		
		if (toState.url.indexOf('adm') > -1) {
			UserService.getLoggedUser().then(function(user) {
				if (!user || !user.administrador) {
					goHome(event);
				}
			});
		}
	});
	
	var goHome = function(event) {
		event.preventDefault();
		$state.go('app.home');
	};
})
;