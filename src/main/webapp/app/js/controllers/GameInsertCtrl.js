angular.module('tenis.controllers')

.controller('GameInsertCtrl', function($scope, GameService, CategoryService, AthleteService) {
	$scope.game = { statusGame:1 };
	$scope.gameHasBeenCreated = false;
	$scope.gameHasBeenValidated = false;
	$scope.gameHasAthleteEquals = false;
	
	$scope.listPlayerOne = function() {
		AthleteService.fetch($scope.game.categoria.id).then(function(data) {
			$scope.players = data;
		});
	};
	
	$scope.create = function() {
		if ($scope.game.adversario.id != $scope.game.apontador.id) {
			GameService.save($scope.game).then(function() {
				$scope.gameHasBeenCreated = true;
			}).catch(function(error) {
				if (!error) {
					$scope.gameHasBeenValidated = true;
				}
			});
		} else {
			$scope.gameHasAthleteEquals = true;
		}
	};
	
	var init= function() {
		CategoryService.fetch().then(function(data) {
			$scope.categories = data;
		});
	};
	
	init();
});