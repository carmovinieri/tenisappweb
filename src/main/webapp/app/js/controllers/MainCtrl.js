angular.module('tenis.controllers')

.controller('MainCtrl', function($scope, CategoryService, UserService, $uibModal) {
	
	$scope.categories = [];
	
	$scope.openLogin = function() {
		$uibModal.open({
			templateUrl: 'template/login.html',
			controller: 'LoginModalCtrl'
		});
	};
	
	// bootstrap 3 navbar fix
	$(document).on('click','.navbar-collapse.in',function(e) {
	    if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
	        $(this).collapse('hide');
	    }
	});
	
	// bootstrap 3 navbar fix
	$(document).on('click','.navbar-brand',function(e) {
	    if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
	        $('.navbar-collapse.in').collapse('hide');
	    }
	});
	
	var init = function() {
		// This data is used by the menu
		CategoryService.fetch().then(function(data) {
			$scope.categories = data;
		});
		
		UserService.getLoggedUser().then(function(data) {
			$scope.user = data;
		});
	};
	
	init();
});