angular.module('tenis.controllers')

.controller('AthleteApprovalManagerCtrl', function($scope, AdminService) {
	
	$scope.athletes = [];
	
	$scope.approve = function(athlete) {
		AdminService.approveAthlete(athlete.id).then(function() {
			var index = $scope.athletes.indexOf(athlete);
			
			if (index > -1) {
				$scope.athletes.splice(index, 1);
			}
		});
	};
	
	var init = function() {
		AdminService.listPendingAthletes().then(function(data) {
			$scope.athletes = data;
		});
	};
	
	init();
});