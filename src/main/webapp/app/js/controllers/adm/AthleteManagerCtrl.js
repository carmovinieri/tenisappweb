angular.module('tenis.controllers')

.controller('AthleteManagerCtrl', function($scope, AdminService, CategoryService, $state, $stateParams) {

	$scope.filter = {
		filterByName: false
	};
	
	$scope.list = function(anchor) {
		AdminService.listAthletes($scope.filter.categoryId).then(function(data) {
			$scope.athletes = data;
		});
	};
	
	$scope.edit = function(athlete) {
		$state.go('app.edit-athlete', {id: athlete.id});
	};
	
	$scope.showNameFilter = function() {
		$scope.filter.filterByName = !$scope.filter.filterByName; 
		$scope.filter.athleteName = '';
	};
	
	var init = function() {
		CategoryService.fetch().then(function(data) {
			$scope.categories = data;
			
			if ($stateParams.categoryId) {
				for (var i = 0; i < $scope.categories.length; i++) {
					if ($scope.categories[i].id == $stateParams.categoryId) {
						$scope.filter.categoryId = $scope.categories[i].id;
						break;
					}
				}
				
				$scope.list();
			}
		});
	};
	
	init();
});