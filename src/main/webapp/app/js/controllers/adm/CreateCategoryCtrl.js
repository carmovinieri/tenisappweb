angular.module('tenis.controllers')

.controller('CreateCategoryCtrl', function($scope, CategoryService) {
	$scope.category = {};
	$scope.categoryHasBeenCreated = false;
	$scope.genders = [{ description: 'Masculino', value: false }, { description : 'Feminino', value: true }];
	
	$scope.create = function() {
		CategoryService.save($scope.category).then(function() {
			$scope.categoryHasBeenCreated = true;
			CategoryService.fetch(true);
		});
	};
});