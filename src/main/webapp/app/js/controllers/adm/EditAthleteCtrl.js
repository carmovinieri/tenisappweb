angular.module('tenis.controllers')

.controller('EditAthleteCtrl', function($scope, AdminService, CategoryService, $state, $stateParams) {
	
	$scope.athlete = {};
	$scope.originalCategoryId = 0;
	$scope.categories = [];
	
	$scope.edit = function() {
		AdminService.updateAthlete($scope.athlete).then(function() {
			$scope.goBack();
		});
	};
	
	$scope.goBack = function() {
		$state.go('app.athlete-manager', {categoryId: $scope.originalCategoryId});
	}
	
	var init = function() {
		CategoryService.fetch().then(function(data) {
			$scope.categories = data;
		});
		
		AdminService.findAthleteById($stateParams.id).then(function(data) {
			$scope.athlete = data;
			$scope.athlete.dateOfBirth = new Date($scope.athlete.dateOfBirth);
			
			$scope.originalCategoryId = data.category.id;
		});
	};
	
	init();
});