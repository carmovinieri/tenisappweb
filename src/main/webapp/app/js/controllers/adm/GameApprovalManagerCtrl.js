angular.module('tenis.controllers')

.controller('GameApprovalManagerCtrl', function($scope, AdminService) {
	
	$scope.pendingGames = [];
	
	$scope.approveGame = function(game) {
		AdminService.approveGame(game.id).then(function() {
			var index = $scope.pendingGames.indexOf(game);
			
			if (index > -1) {
				$scope.pendingGames.splice(index, 1);
			}
		});
	};
	
	$scope.repproveGame = function(game) {
		AdminService.repproveGame(game.id).then(function(){
			var index = $scope.pendingGames.indexOf(game);
			
			if (index > -1) {
				$scope.pendingGames.splice(index, 1);
			}
			
		});
	};
	
	var init = function() {
		AdminService.listPendingGames().then(function(games){
			$scope.pendingGames = games;
		});
	};
	
	init();
});