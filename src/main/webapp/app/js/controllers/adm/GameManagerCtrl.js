angular.module('tenis.controllers')

.controller('GameManagerCtrl', function($scope, GameService, AthleteService, UserService) {
	
	$scope.games = null;
	
	$scope.filter = {};
	
	$scope.selectedImage = {};
	
	$scope.getGames = function() {
		GameService.findGames($scope.filter).then(function(data) {
			$scope.games = data;
		});
	}
});