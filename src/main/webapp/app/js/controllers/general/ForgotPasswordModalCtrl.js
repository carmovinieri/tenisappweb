angular.module('tenis.controllers')

.controller('ForgotPasswordModalCtrl', function($scope, $uibModalInstance, PasswordService) {
	$scope.forgotPassword = {};
	
	$scope.hasMailBeenSent = false;
	
	$scope.sendMail = function() {
		PasswordService.sendForgotPasswordMail($scope.forgotPassword).then(function(response) {
			$scope.hasMailBeenSent = true;
		});
	};
	
	$scope.close = function() {
		$uibModalInstance.close('dismiss');
	};
});