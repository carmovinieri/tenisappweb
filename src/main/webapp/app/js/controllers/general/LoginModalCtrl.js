angular.module('tenis.controllers')

.controller('LoginModalCtrl', function($rootScope, $scope, $uibModal, $uibModalInstance, UserService) {
	
	$scope.loginData = {};
	
	$scope.hasLoginFailed = false;
	
	$scope.login = function() {
		UserService.login($scope.loginData).then(function(data) {
			if (data) {
				$scope.close();
				location.reload();
			}
		}).catch(function(error) {
			if (error.hasLoginFailed) {
				$scope.hasLoginFailed = true;
			}
		});
	};
	
	$scope.openForgotPasswordModal = function() {
		$uibModal.open({
			templateUrl: 'template/forgot-password.html',
			controller: 'ForgotPasswordModalCtrl'
		});
	};
	
	$scope.close = function() {
		$uibModalInstance.close('dismiss');
	};
});