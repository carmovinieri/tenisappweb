angular.module('tenis.controllers')

.controller('ResetPasswordCtrl', function($scope, PasswordService, $stateParams) {
	$scope.athlete = null;
	
	$scope.invalidId = false;
	
	$scope.hasPasswordBeenReset = false;
	
	var id = $stateParams.id;

	$scope.resetPassword = function() {
		PasswordService.resetPassword($scope.athlete, id).then(function(response) {
			$scope.hasPasswordBeenReset = true;
		});
	};
	
	var init = function() {
		PasswordService.checkForgotPasswordIdentifier(id).then(function(data) {
			$scope.athlete = {
				email: data.email
			};
		}).catch(function() {
			$scope.invalidId = true;
		});
	};
	
	init();
});