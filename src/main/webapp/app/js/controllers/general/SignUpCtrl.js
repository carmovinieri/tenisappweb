angular.module('tenis.controllers')

.controller('SignUpCtrl', function($scope, CategoryService, UserService) {
	
	$scope.hasAccountBeenCreated = false;

	$scope.athleteData = {};
	
	$scope.createAccount = function() {
		UserService.createAccount($scope.athleteData).then(function(response) {
			$scope.hasAccountBeenCreated = true;
		});
	}
	
	var init= function() {
		CategoryService.fetch().then(function(data) {
			$scope.categories = data;
		});
	};
	
	init();
});