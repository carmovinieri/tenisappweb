angular.module('tenis.controllers')

.controller('ApproveGameCtrl', function($scope, UserService) {
	$scope.pendingGames = [];
	
	$scope.gameToReprove = {};
	
	$scope.approve = function(game, shouldApprove) {
		UserService.approveGame(game, shouldApprove).then(function() {
			var index = $scope.pendingGames.indexOf(game);
			
			if (index > -1) {
				$scope.pendingGames.splice(index, 1);
			}
		});
	};
	
	$scope.setGameToReprove = function(pendingGame) {
		$scope.gameToReprove = pendingGame;
	}
	
	$scope.$watch('gameToReprove.justificativa', function(newValue, oldValue) {
		if (!newValue && oldValue && oldValue.length >= 2) {
			$scope.gameToReprove.justificativa = oldValue;
		} 
	});

	var init= function() {
		UserService.getLoggedUser().then(function(user) {
			$scope.user = user;
			return UserService.fetchAwaitingApprovalGames(user);
		}).then(function(data) {
			$scope.pendingGames = data;
		});
	};
	
	init();
});