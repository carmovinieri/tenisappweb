angular.module('tenis.controllers')

.controller('ControlPanelCtrl', function($scope, ControlPanelService, UserService, $state) {
	
	$scope.edit = function(user) {
		$state.go('app.my-profile');
	};
	
	var init = function() {
		UserService.getLoggedUser().then(function(user) {
			$scope.user = user;
		});
		
		UserService.loadRankInfo().then(function(perfil){
			$scope.perfil = perfil;
		});
	};
	
	init();
});