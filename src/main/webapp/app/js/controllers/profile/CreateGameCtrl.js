angular.module('tenis.controllers')

.controller('CreateGameCtrl', function($scope, GameService, UserService, AthleteService) {
	$scope.game = { statusGame:0 };
	$scope.gameWHasBeenCreated = false;
	$scope.gameWHasBeenValidated = false;
	
	$scope.create = function() {
		if ($scope.game.adversario.id != $scope.user.id) {
			$scope.game.apontador = $scope.user;
			$scope.game.categoria = $scope.user.category;
			GameService.save($scope.game).then(function() {
				$scope.gameWHasBeenCreated = true;
			}).catch(function(error) {
				if (!error) {
					$scope.gameWHasBeenValidated = true;
				}
			});
		} 
	};
	
	var init= function() {
		UserService.getLoggedUser().then(function(user) {
			$scope.user = user;
		});
		
		AthleteService.getNotPlayInMounth().then(function(data) {
			$scope.players = data;
		});
	};
	
	init();
});