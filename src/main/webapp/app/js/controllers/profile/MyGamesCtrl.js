angular.module('tenis.controllers')

.controller('MyGamesCtrl', function($scope, GameService, UserService) {
	
	$scope.games = null;
	
	$scope.filter = {};
	
	$scope.selectedImage = {};
	
	$scope.getGames = function() {
		$scope.filter.athleteId = $scope.user.id;
		GameService.findGames($scope.filter).then(function(response) {
			$scope.games = response;
		});
	}
	
	var init= function() {
		
		UserService.getLoggedUser().then(function(user) {
			$scope.user = user;
		});
		
		GameService.findUserGames().then(function(data){
			$scope.games = data;
		});
		
	};
	
	init();
});