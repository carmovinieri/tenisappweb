angular.module('tenis.controllers')

.controller('MyProfileCtrl', function($scope, UserService, $state, $window) {
	
	$scope.edit = function() {
		UserService.updateProfile($scope.user).then(function() {
			$state.go('app.control-panel');
		});
	};
	
	var init = function() {
		$window.scrollTo(0, 0);
		
		UserService.getLoggedUser().then(function(user) {
			if (user) {
				UserService.loadProfile().then(function(user) {
					$scope.user = user;
				}).catch(function() {
					$state.go('app.home');
				});
			}
		}).catch(function() {
			$state.go('app.home');
		});
		
	};
	
	init();
});