angular.module('tenis.controllers')

.controller('AthleteListCtrl', function($scope, AthleteService, CategoryService, UserService, $stateParams,$state) {
	
	var categoryId = $stateParams.categoryId;
	
	$scope.athletes = [];
	
	$scope.edit = function(athlete) {
		$state.go('app.edit-athlete', {id: athlete.id});
	};
	
	$scope.approve = function(athlete) {
		AthleteService.approveStatus(athlete);
	}
	
	var init = function() {
		if (categoryId) {
			AthleteService.fetch(categoryId).then(function(data) {
				$scope.athletes = data;
			});
			
			UserService.getLoggedUser().then(function(user) {
				$scope.user = user;
			});
			
			CategoryService.findById(categoryId).then(function(categ){
				$scope.category = categ;
			});
		}
	};
	
	init();
});