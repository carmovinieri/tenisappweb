angular.module('tenis.controllers')

.controller('CategoryListCtrl', function($scope, CategoryService) {
	
	$scope.categories = [];
	
	var init = function() {
		CategoryService.fetch().then(function(response) {
			$scope.categories = response;
		});
	};
	
	init();
	
});