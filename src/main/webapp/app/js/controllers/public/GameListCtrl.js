angular.module('tenis.controllers')

.controller('GameListCtrl', function($scope, GameService, CategoryService) {
	
	$scope.games = null;
	
	$scope.filter = {};
	
	$scope.selectedImage = {};
	
	$scope.statusGame = [
	                  { description: 'APROVADO', value: 1 }, 
	                  { description : 'AG APROVAÇÃO', value: 0 },
	                  { description : 'REPROVADO', value: 2 },
	                  { description : 'PLACAR INVALIDO', value: 3 },
	                  { description : 'DATA FUTURA', value: 5 },
	                  { description : 'ATLETA DESATIVADO', value: 6 },
	                  ];
	
	$scope.gameModal = null;
	
	$scope.getGames = function() {
		GameService.findGames($scope.filter).then(function(response) {
			$scope.games = response;
		});
	}
	
	var init= function() {
		CategoryService.fetch().then(function(data) {
			$scope.categories = data;
		});
	};
	
	init();
});