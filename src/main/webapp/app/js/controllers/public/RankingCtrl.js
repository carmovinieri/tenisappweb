angular.module('tenis.controllers')

.controller('RankingCtrl', function($scope, RankingService, CategoryService, $stateParams) {
	
	$scope.ranking = [];
	
	var init = function() {
		RankingService.fetch($stateParams.categoryId).then(function(data) {
			$scope.ranking = data;
		})
		
		CategoryService.findById($stateParams.categoryId).then(function(categ){
			$scope.category = categ;
		});
	};
	
	init();
});