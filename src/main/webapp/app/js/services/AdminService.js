angular.module('tenis.services')

.factory('AdminService', function($http, $q, $log) {
	
	var service = {
		listAthletes: function(categoryId) {
			var defer = $q.defer();
			
			$http.get('../adm/athlete-list', {
				params: {
					'categoryId': categoryId
				}
			}).then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		listPendingAthletes: function(categoryId) {
			var defer = $q.defer();
			
			$http.get('../adm/pending-athletes').then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		listPendingGames: function(categoryId) {
			var defer = $q.defer();
			
			$http.get('../adm/pending-games').then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		approveAthlete: function(athleteId) {
			var defer = $q.defer();
			
			$http.post('../adm/approve-athlete/' + athleteId).then(function() {
				defer.resolve();
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		approveGame: function(gameId) {
			var defer = $q.defer();
			
			$http.post('../adm/approve-game/' + gameId).then(function() {
				defer.resolve();
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		repproveGame: function(gameId) {
			var defer = $q.defer();
			
			$http.post('../adm/repprove-game/' + gameId).then(function() {
				defer.resolve();
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		findAthleteById: function(athleteId) {
			var defer = $q.defer();
			
			$http.get('../adm/athlete/' + athleteId).then(function(response) {
				defer.resolve(response.data);
			}).catch(function() {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		updateAthlete: function(athlete) {
			var defer = $q.defer();
			
			$http.post('../adm/update-athlete', athlete).then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject(error);
			});
			
			return defer.promise;
		}
	};
	
	return service;
});