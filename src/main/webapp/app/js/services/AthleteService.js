angular.module('tenis.services')

.factory('AthleteService', function($http, $q, $log) {
	
	var user = null;
	var athletes = null;
	
	var service = {
		fetch: function(categoryId) {
			var defer = $q.defer();
			
			$http.get('../atleta/athleteList', {
				params: {
					'categoryId': categoryId
				}
			}).then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		findById: function(id) {
			var defer = $q.defer();
			
			$http.get('../atleta/' + id).then(function(response) {
				var athlete = response.data;
				
				athlete.dateOfBirth = new Date(athlete.dateOfBirth);
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject(error);
			});
		
			return defer.promise;
		},
		
		getNotPlayInMounth: function() {
			var defer = $q.defer();
			
				$http.get('../atleta/getNotPlayInMounth').then(function(response) {
					defer.resolve(response.data);
				}).catch(function(error) {
					$log.error(error);
					defer.reject();
				});
			
			return defer.promise;
		},
		
		update: function(athleteData) {
			var defer = $q.defer();
			
			$http.put('../atleta', athleteData).then(function(response) {
				defer.resolve(response);
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		approveStatus: function(athleteData) {
			var defer = $q.defer();
			
			$http.put('../atleta/approveStatus', athleteData).then(function(response) {
				athleteData.enabled=true;
				defer.resolve(response);
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		perfil: function(id) {
			var defer = $q.defer();
			
			$http.get('../atleta/perfil', {
				params: {
					'idAtleta': id
				}
			}).then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		madeAllGamesMonth: function() {
			var defer = $q.defer();
			
			$http.get('../atleta/madeAllGamesMonth').then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		}
	};
	
	return service;
});