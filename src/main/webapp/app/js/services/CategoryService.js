angular.module('tenis.services')

.factory('CategoryService', function($http, $q) {
	
	var categories = [];
	
	var service = {
		fetch: function(forceReload) {
			var defer = $q.defer();
			
			if (categories.length > 0 && !forceReload) {
				defer.resolve(categories);
			} else {
				$http.get('../categoria').then(function(response) {
					categories = response.data;
					defer.resolve(response.data);
				}).catch(function(error) {
					defer.reject(error);
				});
			}
			
			return defer.promise;
		},
		save: function(category) {
			var defer = $q.defer();
			
			$http.post('../categoria', category).then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				defer.reject(error);
			});
			
			return defer.promise;
		},
		findById: function(id) {
			var defer = $q.defer();
			
			$http.get('../categoria/' + id).then(function(response) {
				var athlete = response.data;
				
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject(error);
			});
		
			return defer.promise;
		},
	};
	
	return service;
});