angular.module('tenis.services')

.factory('ControlPanelService', function($http, $q, $log) {
	
	var user = null;
	var athletes = null;
	
	var service = {
		fetch: function(categoryId) {
			var defer = $q.defer();
			
			$http.get('../controlPanel/athleteListToApprove', {
			}).then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		approveStatus: function(athleteData) {
			var defer = $q.defer();
			
			$http.put('../controlPanel/approveStatus', athleteData).then(function(response) {
				athleteData.enabled=true;
				defer.resolve(response);
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
		
		approveGames: function(gameData) {
			var defer = $q.defer();
			$http.put('../controlPanel/approveGames', gameData).then(function(response){
				defer.resolve(response);
			}).catch(function(error){
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		}
	};
	
	return service;
});