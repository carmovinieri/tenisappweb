angular.module('tenis.services')

.factory('GameService', function($http, $q) {
	
	var service = {
		findGames: function(filter) {
			var defer = $q.defer();
			
			$http.get('../jogo/gameList', {
				params: {
					'inicio' : filter.start,
					'fim' : filter.finish,
					'idCategoria': filter.category,
					'idAtleta' : filter.athleteId,
					'statusGame' : filter.statusGame
				}
			}).then(function(response) {
				defer.resolve(response.data);
			}).catch(function() {
				defer.reject();
			});
			
			return defer.promise;
		},
		save: function(game) {
			var defer = $q.defer();
			
			if (validateScore(game.gamesPro, game.gamesCon)) {
				if (game.foto != null) {
					game.foto = game.foto.base64;
				}
				
				$http.post('../jogo/saveGameWeb', game).then(function(response) {
					defer.resolve(response.data);
				}).catch(function(error) {
					defer.reject(error);
				});
			} else {
				defer.reject();
			}
			
			
			return defer.promise;
		} ,
		
		findUserGames: function(filter) {
			var defer = $q.defer();
			
			$http.get('../jogo/user-games').then(function(response) {
				defer.resolve(response.data);
			}).catch(function() {
				defer.reject();
			});
			
			return defer.promise;
		} ,
		
		findGamesToApprove: function() {
			var defer = $q.defer();
			
			$http.get('../jogo/gameListToApprove', {
				
			}).then(function(response) {
				defer.resolve(response.data);
			}).catch(function() {
				defer.reject();
			});
			
			return defer.promise;
		}
		
	};
	
	var validateScore = function(gamesPro, gamesCon) {
		var isValid = true;

		if (gamesPro > 7 || gamesCon > 7) {
			isValid = false;
		}

		if (gamesPro <=5 && gamesCon <= 5) {
			isValid = false;
		}

		if (gamesPro == 6) {
			if (gamesCon >= 5 && gamesCon < 7) {
				isValid = false;
			}
		}

		if (gamesCon == 6) {
			if (gamesPro >= 5 && gamesPro < 7) {
				isValid = false;
			}
		}

		if (gamesPro == 7) {
			if (!(gamesCon >= 5 && gamesCon < 7)) {
				isValid = false;
			}
		}

		if (gamesCon == 7) {
			if (!(gamesPro >= 5 && gamesPro < 7)) {
				isValid = false;
			}
		}

		//console.log(gamesPro +' x '+gamesCon + '--> ' + isValid);

		return isValid;
	};
	
	return service;
});