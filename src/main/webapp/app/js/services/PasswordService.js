angular.module('tenis.services')

.factory('PasswordService', function($http, $q, $log) {
	
	var service = {
		sendForgotPasswordMail: function(email) {
			var defer = $q.defer();
			
			$http.post('../password/forgot-mail', email).then(function(response) {
				defer.resolve(response);
			}).catch(function(error) {
				$log.error(error);
				defer.reject(error);
			});
			
			return defer.promise;
		},
		
		checkForgotPasswordIdentifier: function(id) {
			var defer = $q.defer();
			
			$http.get('../password/get-mail-identifier', {params: {
				id: id
			}}).then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject(error);
			});
			
			return defer.promise;
		},
		
		resetPassword: function(athlete, resetPasswordId) {
			var defer = $q.defer();
			
			$http.post('../password/reset-password', athlete, {
				params: {
					'resetPasswordId': resetPasswordId
				}
			}).then(function(response) {
				defer.resolve(response);
			}).catch(function(error) {
				$log.error(error);
				defer.reject(error);
			});
			
			return defer.promise;
		}
	};
	
	return service;
});