angular.module('tenis.services')

.factory('RankingService', function($http, $q) {
	var service = {
		fetch: function(categoryId) {
			var defer = $q.defer();
			
			$http.get('../rank', {
				params: {
					'idCategoria': categoryId
				}
			}).then(function(response) {
				defer.resolve(response.data);
			}).catch(function() {
				defer.reject();
			});
			
			return defer.promise;
		}
	};
	
	return service;
});