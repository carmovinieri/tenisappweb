angular.module('tenis.services')

.factory('UserService', function($http, $q, $log) {
	var user = null;
	
	var service = {
		login: function(loginData) {
			var defer = $q.defer();
			
			$http({
				method: 'POST',
				data: $.param(loginData),
				url: '../j_spring_security_check',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).then(function(response) {
				if (response.status == 200) {
					user = response.data;
					defer.resolve(response.data);
				}
			}).catch(function(error) {
				$log.error(error);
				
				if (error.status == 403) {
					error.hasLoginFailed = true;
				}
				defer.reject(error);
			});
			
			return defer.promise;
		},
		
		getLoggedUser: function() {
			var defer = $q.defer();
			
			if (user) {
				defer.resolve(user)
			} else {
				$http.get('../user/getLoggedUser').then(function(response) {
					user = response.data;
					defer.resolve(response.data);
				}).catch(function(error) {
					$log.error(error);
					defer.reject();
				});
			}
			
			return defer.promise;
		},
		
		updateProfile: function(userData) {
			var defer = $q.defer();
			
			$http.post('../user/update-profile', userData).then(function(response) {
				defer.resolve(response);
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		},
			
		createAccount: function(userData) {
			var defer = $q.defer();
			
			$http.post('../atleta/inscrever', userData).then(function(response) {
				defer.resolve(response);
			}).catch(function(error) {
				$log.error(error);
				defer.reject(error);
			});
			
			return defer.promise;
		},
		
		fetchAwaitingApprovalGames: function(user) {
			var defer = $q.defer();
			
			$http.get('../jogo/gameListAproveMobile', {
				params: {
					'athleteId': user.id
				}
			}).then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject(error);
			});
			
			return defer.promise;
		},
		
		approveGame: function(game, shouldApprove) {
			var defer = $q.defer();
			
			if (shouldApprove) {
				game.statusGame = 1;
			} else {
				game.statusGame = 2;
			}
			
			$http.post('../jogo/approveGame', game).then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject(error);
			});
			
			return defer.promise;
		},
		
		loadProfile: function() {
			var defer = $q.defer();
			
			$http.get('../user/load-profile', {
				params: {
					'userId': user.id
				}
			}).then(function(response) {
				var userData = response.data;
				userData.dateOfBirth = new Date(userData.dateOfBirth);
				defer.resolve(userData);
			}).catch(function(error) {
				$log.error(error);
				defer.reject(error);
			});
			
			return defer.promise;
		},
		
		loadRankInfo: function() {
			var defer = $q.defer();
			
			$http.get('../user/load-rank-info').then(function(response) {
				defer.resolve(response.data);
			}).catch(function(error) {
				$log.error(error);
				defer.reject();
			});
			
			return defer.promise;
		}
	};
	
	return service;
});