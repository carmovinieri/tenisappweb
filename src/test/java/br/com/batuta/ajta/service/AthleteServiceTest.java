package br.com.batuta.ajta.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.batuta.ajta.entity.Athlete;
import br.com.batuta.ajta.exception.AjtaException;
import br.com.batuta.ajta.exception.ServiceException;
import br.com.batuta.ajta.exception.RepositoryException;
import br.com.batuta.ajta.repository.AthleteRepository;
import br.com.batuta.ajta.service.AthleteService;
import br.com.batuta.ajta.service.MailService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring/root-context.xml", 
		"file:src/main/webapp/WEB-INF/spring/security-context.xml"})
public class AthleteServiceTest {

	@InjectMocks
	private AthleteService athleteService;
	
	@Mock
	private MailService mailService;

	@Mock
	private AthleteRepository athleteRepository;

	private Athlete athlete;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		
		athlete = new Athlete();
		athlete.setName("John Cena");
		athlete.setEmail("johncena@gmail.com");
	}
	
	@Ignore
	@Test
	public void when_email_is_valid_should_return_athlete() throws AjtaException {
		when(athleteRepository.findByEmail(athlete.getEmail())).thenReturn(athlete);
		
		Athlete john = athleteService.findByEmail(athlete.getEmail());
		
		assertEquals(athlete.getEmail(), john.getEmail());
		assertEquals(athlete.getName(), john.getName());
	}
	
	@Ignore
	@Test
	public void when_email_is_invalid_should_return_null() throws AjtaException {
		when(athleteRepository.findByEmail(athlete.getEmail())).thenReturn(null);
		
		Athlete john = athleteService.findByEmail(athlete.getEmail());
		
		assertEquals(john, null);
	}
	
	@SuppressWarnings("unchecked")
	@Ignore
	@Test(expected = RepositoryException.class)
	public void when_repository_problem_occurs_should_throw_RepositoryException() throws AjtaException {
		when(athleteRepository.findByEmail(athlete.getEmail())).thenThrow(RepositoryException.class);
		
		athleteService.findByEmail(athlete.getEmail());
	}

	@SuppressWarnings("unchecked")
	@Ignore
	@Test(expected = ServiceException.class)
	public void when_service_problem_occurs_should_throw_ServiceException() throws AjtaException {
		when(athleteRepository.findByEmail(athlete.getEmail())).thenThrow(Exception.class);
		
		athleteService.findByEmail(athlete.getEmail());
	}
	
}
